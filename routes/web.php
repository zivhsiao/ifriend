<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SignController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\TestController;

Route::get('/', [HomeController::class, 'index']);
Route::get('intro', [HomeController::class, 'intro']);
Route::get('help', [SignController::class, 'help']);

// 登入以及登出
Route::get('sign-up', [SignController::class, 'sign_up']);
Route::get('sign-in', [SignController::class, 'sign_in']);
Route::post('user/sign-up', [SignController::class, 'check_sign_up']);
Route::post('user/sign-in', [SignController::class, 'check_sign_in']);
Route::get('logout', [SignController::class, 'logout']);

// 題目
Route::get('topic', [TopicController::class, 'topic']);
Route::get('topic/request', [TopicController::class, 'request']);

// 學習，練習，測驗 
Route::get('topic/learn', [TopicController::class, 'learn']);
Route::get('topic/train', [TopicController::class, 'train']);
Route::get('topic/exam', [TopicController::class, 'exam']);

// 存檔
Route::post('ajax/save_server', [AjaxController::class, 'save_server']);
Route::post('ajax/save_audio/{topic}/{order}', [AjaxController::class, 'save_audio']);

// 刪除記錄
Route::post('ajax/delete_server', [AjaxController::class, 'delete_server']);

// 讀取學習的的最後一題
Route::post('ajax/get_question_last', [HomeController::class, 'get_question_last']);

// check audio
Route::post('ajax/check_audio/{topic}/{order}', [AjaxController::class, 'check_audio']);

Route::post('ajax/check_notify', [AjaxController::class, 'check_notify']);
Route::post('ajax/save_voice_to_text/{topic}/{order}', [AjaxController::class, 'save_voice_to_text']);

Route::post('ajax/post_log', [AjaxController::class, 'post_log']);

Route::post('ajax/all_group', [AjaxController::class, 'all_group']);

Route::get('ajax/question_re_cal', [AjaxController::class, 'question_re_cal']);

// Test
Route::get('test', [TestController::class, 'test']);

Route::group(['prefix' => 'backend'], function () {
    Voyager::routes();
});
