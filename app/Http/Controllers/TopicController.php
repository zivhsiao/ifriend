<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TopicController extends Controller
{
 
    public function __construct()
    {
        
    }

    public function topic(Request $request){

        $login_account = $request->session()->get('account');

        if($login_account == ''){
            return redirect('/');
        } else {
            return view('topic.topic');
        }     
    }


    public function learn(Request $request){

        $login_account = $request->session()->get('account');

        if($login_account == ''){
            return redirect('/');
        } else {
            return view('exam.learn');
        }     
    }

    public function train(Request $request){

        $login_account = $request->session()->get('account');

        if($login_account == ''){
            return redirect('/');
        } else {
            return view('exam.train');
        }     
    }

    public function exam(Request $request){

        $login_account = $request->session()->get('account');

        if($login_account == ''){
            return redirect('/');
        } else {
            return view('exam.exam');
        }     
    }


    public function request(Request $request){

        $login_account = $request->session()->get('account');

        if($login_account == ''){
            return redirect('/');
        } else {
            
            $topic = $request->input('tid');
            $order = $request->input('order');

            $data = DB::table('questions')
                        ->where([
                            ['question_type', '=', $topic],
                            ['order', '=', $order]
                        ])
                        ->orderBy('order', 'asc')
                        ->get();

            $count = DB::table('questions')
                    ->where([
                        ['question_type', '=', $topic],
                    ])
                    ->count();
            

            $dataArr = json_decode($data, true);

            $url = "tid=" . $topic . "&order="; 


            if($dataArr[0]['order'] < $count){
                $dataArr['next_page'] = $url . ($dataArr[0]['order'] + 1);
                $dataArr['prev_page'] = $url . ($dataArr[0]['order'] - 1);
            }

            // 只有判斷第一筆        
            if($dataArr[0]['order'] == 1){
                if($count > 1){
                    $dataArr['next_page'] = $url . "2";
                } else {
                    $dataArr['next_page'] = '';
                }
                $dataArr['prev_page'] = ''; 
            }

            if($dataArr[0]['order'] == $count){
                $dataArr['next_page'] = '';
                $dataArr['prev_page'] = $url . ($dataArr[0]['order'] - 1);
            }  

            $dataArr['count'] = $count;
            
            
            return $dataArr;
        }     
    }
}
