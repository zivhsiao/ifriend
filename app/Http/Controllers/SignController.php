<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Member;
use App\Models\LogMember;

class SignController extends Controller
{
    public function sign_in(Request $request){
        
        return view('home.sign_in');
    }

    
    public function sign_up(Request $request)
    {

        return view('home.sign_up');
    }

    public function help(Request $request)
    {

        return view('home.help');
    }

    public function check_sign_up(Request $request)
    {
        $request->validate([
            'account'       =>  'required',
            'password'      =>  'required',
            'name'          =>  'required',
            'school'        =>  'required',
            'grade'         =>  'required',
            'class'         =>  'required',
            'birthday'      =>  'required',
            'parent_name'   =>  'required',
            'phone'         =>  'required',
        ]);

        $get_user = Member::where('account', $request->input('account'))->get();

        if($get_user->count() == 0){
            DB::table('members')->insert([
                'account'       =>  trim($request->input('account')),
                'password'      =>  bcrypt($request->input('password')),
                'name'          =>  trim($request->input('name')),
                'school'        =>  trim($request->input('school')),
                'grade'         =>  trim($request->input('grade')),
                'class'         =>  trim($request->input('class')),
                'birthday'      =>  trim($request->input('birthday')),
                'parent_name'   =>  trim($request->input('parent_name')),
                'phone'         =>  trim($request->input('phone')),
                'created_at'    =>  date('Y-m-d H:i')
            ]);

            return 'success';
        } else {
            return 'failed';
        }
 
    }

    public function check_sign_in(Request $request)
    {

        $data = $request->all();
        $get_user = DB::table('members')->where([
                            ['account', '=', $data['account']]
                            ])
                            ->first();                    
                    
        if(Hash::check($data['password'], $get_user->password)){


            $request->session()->put('account', $get_user->account);
            $request->session()->put('account_id', $get_user->id);
            $request->session()->put('log_times', $get_user->log_times + 1);

            $update = [
                'member_id'     =>  $get_user->id,
                'log_date'      =>  date('Y-m-d H:i'),
                'comment'       =>  '登入系統',
                'created_at'    => date('Y-m-d H:i')
            ];

            LogMember::insert($update);

            // 寫回給自己的 log_times
            $where = [
                'id'    => $get_user->id
            ];
            $update = [
                'log_times' => $get_user->log_times + 1
            ];
            Member::where($where)->update($update);            

            return 'success';
        } else {
            
            $request->session()->forget('account');
            $request->session()->forget('account_id');



            return 'failed';
        }
 
    }

    public function logout(Request $request) 
    {  

        $update = [
            'member_id'     =>  $request->session()->get('account_id'),
            'log_date'      =>  date('Y-m-d H:i'),
            'comment'       =>  '登出系統',
            'created_at'    => date('Y-m-d H:i')
        ];

        LogMember::insert($update);

        $request->session()->forget('account');
        $request->session()->forget('account_id');

        return redirect('/');
    }
}
