<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Question;
use App\Models\MemberQuestion;
use App\Models\Notify;
use App\Models\MemberTranscript;
use App\Models\MemberResult;
use App\Models\MemberQuestionLog;

class AjaxController extends Controller
{

    /**
     * save_server
     * 儲存並且計算該題成績
     *
     * @param  mixed $request
     * @return void
     */
    public function save_server(Request $request)
    {

        $req = $request->all();

        $account = $request->session()->get('account');
        $account_id = $request->session()->get('account_id');
        $log_times =  $request->session()->get('log_times');

        // 所有分數的權重
        $qAll = Question::where('question_type', $req['topic'])->get();
        $qAllCount = $qAll->count();


        if($req['type_01'] == null){
            $req['type_01'] = '';
        }
        if($req['type_02'] == null){
            $req['type_02'] = '';
        }
        if($req['type_03'] == null){
            $req['type_03'] = '';
        }

        $where = [
            "member_id"     => $account_id,
            "question_id"   => $req['order'],
            "question_type" => $req['topic'],
            'level'         => $req['level'],
        ];
        
        $check = MemberQuestion::where($where)->get();

        if($check->count() == 0){
            $result = [
                "member_id"     => $account_id,
                "question_id"   => $req['order'],
                "question_type" => $req['topic'],
                "type_01"       => $req['type_01'],
                "type_02"       => $req['type_02'],
                "type_03"       => $req['type_03'],
                "order_time"       => $req['order_time'],
                "ttl_time"       => $req['ttl_time'],
                'level'         => $req['level'],
                'log_times'     => $log_times,
                'created_at'    => date('Y-m-d H:i'),
                'updated_at'    => date('Y-m-d H:i')
            ];
            MemberQuestion::insert($result);
        } else {
            $where = [
                "member_id"     => $account_id,
                "question_id"   => $req['order'],
                "question_type" => $req['topic'],
                'level'         => $req['level']
            ];

            $update = [
                'type_01'   => $req['type_01'],
                'type_02'   => $req['type_02'],
                'type_03'   => $req['type_03'],
                "order_time"       => $req['order_time'],
                "ttl_time"       => $req['ttl_time'],
                'log_times'     => $log_times,
                'updated_at'    => date('Y-m-d H:i')
            ];

            MemberQuestion::where($where)->update($update);
        }

        // 單題的處理
        $where = [
            "order"   => $req['order'],
            "question_type" => $req['topic'],
        ];
        
        $question = Question::where($where)->get();
        
        $where = [
            "member_id"     => $account_id,
            "question_id"   => $req['order'],
            "question_type" => $req['topic'],
            'level'         => $req['level'],
            // 'log_times'     => $log_times,
        ];
        $mQuestion = MemberQuestion::where($where)->orderBy('score', 'desc')->first();
    
        $t = 0.0;
        if($req['level'] == 1){
            $where = [
                'question_type' => $req['topic'],
                'order'         => $req['order']
            ];
            $question = Question::where($where)->get();

            $en = trim($question[0]->en_title);
            $en2 = trim($question[0]->en_title2);
        
            $speech_1 = trim($mQuestion->speech_1);
            $speech_2 = trim($mQuestion->speech_2);
            $type_01 = trim($mQuestion->type_01);
            $type_02 = trim($mQuestion->type_02);
            $type_03 = trim($mQuestion->type_03);


            if($en2){
                $en = $en2;
            }

            // voice
            if($speech_1 != ''){
                if($en == $speech_1){
                    $t += 0.2;
                }
            }
            if($speech_2 != ''){
                if($en == $speech_2){
                    $t += 0.2;
                }
            }

            // typing
            if($type_01 != ''){
                if($en == $type_01){
                    $t += 0.2;
                }
            }
            if($type_02 != ''){
                if($en == $type_02){
                    $t += 0.2;
                }
            }
            if($type_03 != ''){
                if($en == $type_03){
                    $t += 0.2;
                }
            }

            if($t > 1.0){
                $t = 1;
            }

            $t = $t * (100 / $qAllCount);
        }

        if($req['level'] == 2){
            $where = [
                'question_type' => $req['topic'],
                'order'         => $req['order']
            ];
            $question = Question::where($where)->get();

            $en = trim($question[0]->en_title);
            $en2 = trim($question[0]->en_title2);
            $speech_1 = trim($mQuestion->speech_1);
            $speech_2 = trim($mQuestion->speech_2);
            $type_01 = trim($mQuestion->type_01);
            $type_02 = trim($mQuestion->type_02);
            $type_03 = trim($mQuestion->type_03);

            // var_dump($speech_1, $speech_2, $type_01, $type_02, $type_03);

            if($en2){
                $en = $en2;
            }
            
            // voice
            if($speech_1 != ''){
                if($en == $speech_1){
                    $t += 0.33;
                }
            }
            // if($speech_2 != ''){
            //     if($en == $speech_2){
            //         $t += 0.25;
            //     }
            // }

            // typing
            if($type_01 != ''){
                if($en == $type_01){
                    $t += 0.33;
                }
            }
            if($type_02 != ''){
                if($en == $type_02){
                    $t += 0.33;
                }
            }
            // if($type_03 != ''){
            //     if($en == $type_03){
            //         $t += 0.167;
            //     }
            // }

            if($t == 0.99){
                $t = 1;
            }

            $t = $t * (100 / $qAllCount);
        }

        if($req['level'] == 3){
            $where = [
                'question_type' => $req['topic'],
                'order'         => $req['order']
            ];
            $question = Question::where($where)->get();

            $en = trim($question[0]->en_title);
            $en2 = trim($question[0]->en_title2);
            $speech_1 = trim($mQuestion->speech_1);
            $speech_2 = trim($mQuestion->speech_2);
            $type_01 = trim($mQuestion->type_01);
            $type_02 = trim($mQuestion->type_02);
            $type_03 = trim($mQuestion->type_03);

            // var_dump($speech_1, $speech_2, $type_01, $type_02, $type_03);

            if($en2){
                $en = $en2;
            }
            
            // voice
            if($speech_1 != ''){
                if($en == $speech_1){
                    $t += 0.5;
                }
            }
            // if($speech_2 != ''){
            //     if($en == $speech_2){
            //         $t += 0.25;
            //     }
            // }

            // typing
            if($type_01 != ''){
                if($en == $type_01){
                    $t += 0.5;
                }
            }
            // if($type_02 != ''){
            //     if($en == $type_02){
            //         $t += 0.33;
            //     }
            // }
            // if($type_03 != ''){
            //     if($en == $type_03){
            //         $t += 0.167;
            //     }
            // }

            $t = $t * (100 / $qAllCount);
        }
        
        $where = [
            'member_id' => $account_id,
            'type'      => $req['topic'],
            'order'     => $req['order'],
            'level'     => $req['level'],
            // 'log_times' => $log_times,
        ];
        $transcript = MemberTranscript::where($where)->get();


        if($transcript->count() == 0){
            $insert = [
                "member_id"     => $account_id,
                "type"          => $req['topic'],
                "order"         => $req['order'],
                'level'         => $req['level'],
                'score'         => $t,
                'log_times'     => $log_times,
                'created_at'    => date('Y-m-d H:i')
            ];
            MemberTranscript::insert($insert);
        } else {
            $where = [
                "member_id"     => $account_id,
                "type"          => $req['topic'],
                "order"         => $req['order'],
                'level'         => $req['level'],
            ];

            $update = [
                'score'         => $t,
                'log_times'     => $log_times,
                'updated_at'    => date('Y-m-d H:i')
            ];

            MemberTranscript::where($where)->update($update);
        }

        // 寫回 member_questions

        $where = [
            "member_id"     => $account_id,
            "question_id"   => $req['order'],
            "question_type" => $req['topic'],
            'level'         => $req['level'],
            'log_times'     => $log_times,
        ];

        $update = [
            'score' => $t
        ];
        
        $result = MemberQuestion::where($where)->update($update);

        // // 寫入會員的成績
        // $where = [
        //     'question_type' => $topic
        // ];

        // $count = Question::where($where)->count();

        // $where = [
        //     "member_id"     => $account_id,
        //     "question_type" => $topic,
        //     'level' => $req['level'],
        // ];

        // $member_sum = MemberQuestion::where($where)->sum('score');


        // $insert = [
        //     "member_id"     => $account_id,
        //     "question_type" => $topic,
        //     'level' => $req['level'],
        //     'score' => $member_sum,
        //     'created_at' => date('Y-m-d H:i'),
        //     'updated_at' => date('Y-m-d H:i')
        // ];
        // MemberResult::insert($insert);
        
        return 'score: ' . $t;

    }

        
    /**
     * get_transaction
     * 取得成績
     *
     * @param  mixed $request
     * @return void
     */
    public function get_transaction(Request $request){

        $req = $request->all();

        $topic = $req['topic'];
        $order = $req['order'];
        $level = $req['level'];
        $member_id = $req['member_id'];

        $where = [
            'member_id' => $member_id,
            'type'      => $topic,
            'order'     => $order,
            'level'     => $level
        ];
        $transcript = MemberTranscript::where($where)->get();
        
        if($transcript->count() > 0){
            $score = $transcript[0]->score;
        } else {
            $score = 0;
        }

        return $score;
    }

  
    /**
     * save_voice_to_text
     * 語音轉文字的儲存
     *
     * @param  mixed $request
     * @param  mixed $topic
     * @param  mixed $order
     * @return void
     */
    public function save_voice_to_text(Request $request, $topic, $order){

        $req = $request->all();
        $number = $req['number'];
        $account_id = $request->session()->get('account_id');

        $where = [
            "member_id"     => $account_id,
            "question_id"   => $order,
            "question_type" => $topic,
            'level' => $req['level'],
        ];

        $check = MemberQuestion::where($where)->get();

        
        if($check->count() == 0){
            $result = [
                "member_id"     => $account_id,
                "question_id"   => $order,
                "question_type" => $topic,
                'level'     => $req['level'],
                'speech_' . $number => $req['transcript'],
                'created_at'    => date('Y-m-d H:i')
            ];
            $result = MemberQuestion::insert($result);
        } else {
            $where = [
                "member_id"     => $account_id,
                "question_id"   => $order,
                "question_type" => $topic,
                'level'     => $req['level'],
            ];

            $update = [
                'speech_' . $number => $req['transcript'],
                'updated_at'    => date('Y-m-d H:i')
            ];

            $result = MemberQuestion::where($where)->update($update);
        }

        return $result;
    }


    /**
     * check_notify
     * 檢查通知訊息
     *
     * @param  mixed $request
     * @return void
     */
    public function check_notify(Request $request){

        $req = $request->all();

        $today = date('Y-m-d H:i');

        $account_id = $request->session()->get('account_id');

        

        $result = DB::select("select * from `notifies` 
                              where (`member_id` = " . $account_id . " or `member_id` IS NULL)
                              and (`start_date` <= '" . $today . "' and `end_date` >= '" . $today . "')");
        
        return response()->json($result);                 

    }

    public function delete_server(Request $request){
    
        $req = $request->all();

        $account = $request->session()->get('account');
        $account_id = $request->session()->get('account_id');
    
        // 塞回 log
        $where = [
            'member_id' => $account_id,
            'level' => $req['level']
        ];
        $member_question = MemberQuestion::where($where)->get()->toArray();
        foreach($member_question as $row){
            $insert = $row;
            unset($insert['id']);

            $insert['created_at'] = date('Y-m-d H:i', strtotime($insert['created_at']));
            $insert['updated_at'] = date('Y-m-d H:i', strtotime($insert['updated_at']));

            MemberQuestionLog::insert($insert);
        }

        $where = [
            'member_id' => $account_id,
            'level' => $req['level']
        ];
        // MemberQuestion::where($where)->delete();
        // MemberTranscript::where($where)->delete();
    }


    /**
     * check_audio
     * 檢查語音檔(暫時用不到)
     * @param  mixed $request
     * @param  mixed $topic
     * @param  mixed $order
     * @return void
     */
    public function check_audio(Request $request, $topic, $order){

        $req = $request->all();

        $publicPath = realpath(public_path());
        $account_id = $request->session()->get('account_id');
        $path = $publicPath . '/upload/audio/' . $account_id . '/' . $topic . "/" . $order;

        if(is_file($path . "/audio_1.mp3")){
            $file_1 = true;
        } else {
            $file_1 = false;
        }

        if(is_file($path . "/audio_2.mp3")){
            $file_2 = true;
        } else {
            $file_2 = false;
        }

        $status = [
            'status' => [
                'audio_1' => $file_1,
                "audio_2" => $file_2
            ]
        ];

        return $status;
    }

    /**
     * 儲存語音(暫時用不到)
     */
    public function save_audio(Request $request, $topic, $order){

        $req = $request->all();

        $publicPath = realpath(public_path());
        $account_id = $request->session()->get('account_id');

        if(!is_dir($publicPath . "/upload")){
            mkdir($publicPath . '/upload', 0777, true);
        }
        if(!is_dir($publicPath . "/upload/audio")){
            mkdir($publicPath . '/upload/audio', 0777, true);
        }
        if(!is_dir($publicPath . "/upload/audio/" . $account_id)){
            mkdir($publicPath . '/upload/audio/' . $account_id, 0777, true);
        }
        if(!is_dir($publicPath . "/upload/audio/" . $account_id . '/' . $topic)){
            mkdir($publicPath . '/upload/audio/' . $account_id . '/'. $topic, 0777, true);
        }
        if(!is_dir($publicPath . "/upload/audio/" . $account_id . '/' . $topic . "/" . $order)){
            mkdir($publicPath . '/upload/audio/' . $account_id .'/' . $topic . "/" . $order, 0777, true);
        }        

        unlink($publicPath . '/upload/audio/' . $account_id . '/' . $topic . "/" . $order . "/" . $req['file'] . ".mp3");

        $mv = move_uploaded_file($_FILES['audio']['tmp_name'], $publicPath . '/upload/audio/' . $account_id . '/' . $topic . "/" . $order . "/" . $req['file'] . ".mp3");
        
    
        $where = [
            "member_id"     => $account_id,
            "question_id"   => $order,
            "question_type" => $topic
        ];
        
        $check = MemberQuestion::where($where)->get();

        if($check->count() == 0){
            $result = [
                "member_id"     => $account_id,
                "question_id"   => $order,
                "question_type" => $topic,
                'created_at'    => date('Y-m-d H:i')
            ];
            MemberQuestion::insert($result);
        } else {
            $where = [
                "member_id"     => $account_id,
                "question_id"   => $order,
                "question_type" => $topic,
            ];

            $update = [
                'updated_at'    => date('Y-m-d H:i')
            ];

            MemberQuestion::where($where)->update($update);
        }



        return 'success';
    }

        
    /**
     * all_group
     *
     * @param  mixed $request
     * @return void
     */
    public function all_group(Request $request){
        $req = $request->all();

        $level = $req['level'];
        $question_type = $req['question_type'];

        $where = [
            // 'level' => $level,
            'question_type' => $question_type,
            // 'member_id' => $request->session()->get('account_id')
        ];
        $result = Question::where($where)->orderBy('order', 'asc')->get();
        $all_score = 0;
        foreach($result as $row){

            $where = [
                'level' => $level,
                'question_type' => $question_type,
                'question_id' => $row['order']
            ];

            $check = MemberQuestion::where($where)->orderBy('score', 'desc')->first();

            if($check != NULL){
                $row['all_score'] += $check->score;
                $row['order_id'] = $row['id'];
                $row['type_01'] = $check->type_01;
                $row['type_02'] = $check->type_02;
                $row['type_03'] = $check->type_03;
                $row['speech_01'] = $check->speech_01;
                $row['speech_02'] = $check->speech_02;

                $row['type_message_01'] = '';
                $row['type_message_02'] = '';
                $row['type_message_03'] = '';
                $row['speech_message_01'] = '';
                $row['speech_message_02'] = '';

                if($row['en_title2']){
                    if($row['en_title2'] !== $check->type_01){
                        $row['type_message_01'] = 'error';
                    }
                    if($row['en_title2'] !== $check->type_02){
                        $row['type_message_02'] = 'error';
                    }
                    if($row['en_title2'] !== $check->type_03){
                        $row['type_message_03'] = 'error';
                    }
                    if($row['en_title2'] !== $check->speech_01){
                        $row['speech_message_01'] = 'error';
                    }
                    if($row['en_title2'] !== $check->speech_02){
                        $row['speech_message_02'] = 'error';
                    }
                } else {
                    if($row['en_title'] !== $check->type_01){
                        $row['type_message_01'] = 'error';
                    }
                    if($row['en_title'] !== $check->type_02){
                        $row['type_message_02'] = 'error';
                    }
                    if($row['en_title'] !== $check->type_03){
                        $row['type_message_03'] = 'error';
                    }
                    if($row['en_title'] !== $check->speech_01){
                        $row['speech_message_01'] = 'error';
                    }
                    if($row['en_title'] !== $check->speech_02){
                        $row['speech_message_02'] = 'error';
                    }
                }

                
            } else {
                $row['order_id'] = -1;    
            }

            
            
        }

        // $result['all_score'] = $all_score;
        
        return $result;

    }

    
    /**
     * post_log
     *
     * @param  mixed $request
     * @return void
     */
    public function post_log(Request $request){
        
        $req = $request->all();

        $where = [
            'question_type' => $req['question_type'],
        ];

        $count = Question::where($where)->orderBy('order', 'desc')->first();


        $where = [
            'level' => $req['level'],
            'member_id' => $request->session()->get('account_id'),
            'question_type' => $req['question_type']
        ];
        $check = MemberQuestion::where($where)
                                ->orderBy('question_id', 'desc')
                                ->first();

        $result = [
            'result' => 0,
        ];
        

        if($check->count() > 0){
            $result = [
                'result' => 1,
                'level' => $req['level'],
                'question_type' => $check->question_type,
                'order'  => $check->question_id,
                'last_order' => ($check->question_id == $count->order ? 1: 0)
            ];
        } 

        

        return $result;
    }

    
    /**
     * question_re_cal
     * 重新計算
     * @param  mixed $request
     * @return void
     */
    public function question_re_cal(Request $request){

        $question_arr = MemberQuestion::get();

        foreach($question_arr as $row){

            $question_type = $row->question_type;
            $question_id = $row->question_id;

            $question_count = Question::where('question_type', $question_type)->count();
            // 計算有多少題目
            $t = 100 / $question_count;

            $where = [
                'question_type' => $question_type,
                'order'            => $question_id
            ];

            $question_single = Question::where($where)->first();

            if($question_single != NULL){
                $question = 0;            
                if(trim($row->speech_01) == $question_single->en_title){
                    $question += 0.2;
                };
                if(trim($row->speech_02) == $question_single->en_title){
                    $question += 0.2;
                };
                
                if(trim($row->type_01) == $question_single->en_title){
                    $question += 0.2;
                };
                if(trim($row->type_02) == $question_single->en_title){
                    $question += 0.2;
                };
                if(trim($row->type_03) == $question_single->en_title){
                    $question += 0.2;
                };
    
                $tt = $question * $t;
    
                $where = [
                    'question_type' => $row->question_type,
                    'question_id' => $row->question_id,
                    'member_id' => $row->member_id
                ];
    
                $update = [
                    'score' => $tt
                ];
                
                MemberQuestion::where($where)->update($update);
            }
            
        }


    }
    
}
