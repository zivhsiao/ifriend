<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MemberQuestion;
use App\Models\Member;

class HomeController extends Controller
{    
    /**
     * index
     * 首頁
     * 
     * @param  mixed $request
     * @return void
     */
    public function index(Request $request)
    {

        $login_account = $request->session()->get('account');

        if($login_account == ''){
            
            return view('home.index');
        } else {

            return redirect('/topic');
            
        }
    }

    public function intro()
    {

        return view('home.intro');
    }

    
    /**
     * get_question_last
     * 每個主題最後一題
     * 
     * @param  mixed $request
     * @return void
     */
    public function get_question_last(Request $request){

        $input = $request->all();

        $member_id = $request->session()->get('account_id');
        $question_type = $input['question_type'];
        $level = $input['level'];

        $res = MemberQuestion::where('member_id', $member_id)->where('question_type', $question_type)
                                    ->orderBy('question_id', 'desc')->first();

        $result = [
            'result' => $res->question_id ?? 1,
        ];

        return response()->json($result, 200);                            

    }
}
