<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
    <link rel="stylesheet" href="{{ asset('css/test.css') }}">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>

<body>

    <button id="speakButton" onclick="speak()"></button>

    <script>
        function speak() {
            speechSynthesis.speak(new SpeechSynthesisUtterance("哈嘍，你好嗎？"));
        }
        // speak();


        $(function(){
            $('#speakButton').hide();
            $('#speakButton').click();
        })
    </script>

</html>