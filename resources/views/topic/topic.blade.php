@extends('layouts.app')

@section('title', '題目選擇')

@section('content')
<div class="container">
    <div class="clearfix mt-4">
        <div class="float-left">
            <label id="lblMode" class="my-3 font-size-40" style="display:none;"></label>
        </div>
        <div class="float-right">
            <button type="button" id="btnMode" class="btn btn-outline-primary btn-lg mr-2" style="display:none;"
                onclick="mode_enter()">模式</button>
            <button type="button" id="btnSignOut" class="btn btn-outline-primary btn-lg"
                onclick="user_signOut()">登出</button>
        </div>
    </div>

    <div id="divMode" class="col-sm-12 text-center">
        <!-- <div class="col-sm-3 mx-auto">
                <h3>選擇模式</h3>
            </div> -->

        <div class="row mt-5">
            <button id="learn_mode"
                class="btn btn-success col-sm col-12 my-2 mr-sm-3 font-size-60 btn_click"
                style="height:200px; color: white" onclick="mode_enter(0)">學習模式</button>
            <button id="train_mode"
                class="btn  btn-warning btn- col-sm col-12 my-2 mx-sm-3 font-size-60 btn_click"
                style="height:200px; color: white" onclick="mode_enter(1)">練習模式</button>
            <button id="exam_mode"
                class="btn  btn-danger col-sm col-12 my-2 ml-sm-3 font-size-60 btn_click"
                style="height:200px; color: white" onclick="mode_enter(2)">測驗模式</button>
        </div>
    </div>

    <div id="divTopic" class="col-sm-12 text-center" style="display:none;">
        <div class="row mt-3">
            <button class="btn btn-outline-primary col-sm col-12 my-2 mr-sm-3 font-size-60 buttton_click"
                style="height:200px;" onclick="topic_select(0)">題組 - 1</button>
            <button class="btn btn-outline-primary col-sm col-12 my-2 mx-sm-3 font-size-60 buttton_click"
                style="height:200px;" onclick="topic_select(1)">題組 - 2</button>
            <button class="btn btn-outline-primary col-sm col-12 my-2 ml-sm-3 font-size-60 buttton_click"
                style="height:200px;" onclick="topic_select(2)">題組 - 3</button>
        </div>
        <div class="row mt-2">
            <button class="btn btn-outline-primary col-sm col-12 my-2 mr-sm-3 font-size-60 buttton_click"
                style="height:200px;" onclick="topic_select(3)">題組 - 4</button>
            <button class="btn btn-outline-primary col-sm col-12 my-2 mx-sm-3 font-size-60 buttton_click"
                style="height:200px;" onclick="topic_select(4)">題組 - 5</button>
            <button class="btn btn-outline-primary col-sm col-12 my-2 ml-sm-3 font-size-60 buttton_click"
                style="height:200px;" onclick="topic_select(5)">題組 - 6</button>
        </div>
        <div class="row mt-2">
            <button class="btn btn-outline-primary col-sm col-12 my-2 mr-sm-3 font-size-60 buttton_click"
                style="height:200px;" onclick="topic_select(6)">題組 - 7</button>
            <button class="btn btn-outline-primary col-sm col-12 my-2 mx-sm-3 font-size-60 buttton_click"
                style="height:200px;" onclick="topic_select(7)">題組 - 8</button>
            <button class="btn btn-outline-primary col-sm col-12 my-2 ml-sm-3 font-size-60 buttton_click"
                style="height:200px;" onclick="topic_select(8)">題組 - 9</button>
        </div>
    </div>
</div>


<div id="notify_show" class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLongTitle">通知</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table style="width: 100%; font-size: 26px;">
                    <thead>
                        <tr>
                            <th>內容</th>
                            <th>時間</th>

                        </tr>
                    </thead>
                    <tbody id="notify_message">
                        <tr>
                            <td></td>
                            <td></td>

                        </tr>
                    </tbody>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">關 閉</button>

            </div>
        </div>
    </div>
</div>

<embed id="media" src="" width="180" height="90" loop="false" autostart="true" hidden="true" />
<audio id="media_mp3" src="" controls autoplay hidden></audio>

<script type="text/javascript">
    var modeSelected = false;
    var modeIndex = -1;
    var modeList = ['學習模式', '練習模式', '測驗模式'];
    var topicSelected = false;
    var topicIndex = -1;
    var topicList = ['題組1', '題組2', '題組3', '題組4', '題組5', '題組6', '題組7', '題組8', '題組9']
    
    $(function() {
        
        // responsiveVoice.stop();

        // mode_enter();

        notify();

    });

    function notify(){

        responsiveVoice.enableWindowClickHook();
        responsiveVoice.clickEvent();
        responsiveVoice.setDefaultVoice("Chinese Female");
        
        // $.post("{{ url('ajax/check_notify') }}", 
        //     function(data){
        //         alert(data);
        //         $('#notify_show').modal('show');
        // });

        let voice = '';
        $.post("{{ url('ajax/check_notify') }}", function(){
            // console.log('success')
        }).done(function(res){
            result = '';
            $.each(res, function(i, val){
                result += '<tr style="border: 1px solid #ccc"><td>' + val.content + '</td><td>' + val.start_date + '<br>' + val.end_date + '</td></tr>';

                voice += val.content;
                
            })
            $('#notify_message').empty();
            $('#notify_message').html(result)
            $('#notify_show').modal('show');
            delay().then(function(){
                $('#media').attr('src', '/sound/notify2.wav');
                $('#media_mp3').attr("src", '/sounds/notify2.wav');
                return delay(2000)
            }).then(function(){
                responsiveVoice.speak(voice , "Chinese Female");
            })
        }).always(function(data){
            // console.log(data);
        })
    }

    let delay = function(s){
        return new Promise(function(resolve,reject){
            setTimeout(resolve,s); 
        });
    };

    $(document).on('keypress', function(e) {
        console.log(e);

        if(e.code === 'KeyQ' && e.ctrlKey) { 
            // console.log('page_back');
            window.location.href = '/';
        }
    });

    responsiveVoice.enableWindowClickHook();
    responsiveVoice.clickEvent();
    responsiveVoice.setDefaultVoice("Chinese Female");

    $(document).on('mouseover', '#learn_mode', function(e) {
        mode_select(0);
    })
    
    $(document).on('mouseover', '#train_mode', function(e) {
        mode_select(1);
    })

    $(document).on('mouseover', '#exam_mode', function(e) {
        mode_select(2);
    })

    $(document).on('mouseover', '#btnSignOut', function(e) {
        responsiveVoice.speak('您選擇的是登出' , "Chinese Female");
    })

    // $(document).on('focus', '#learn_mode', function(e) {
    //     mode_select(0);
    // })
    
    // $(document).on('focus', '#train_mode', function(e) {
    //     mode_select(1);
    // })

    // $(document).on('focus', '#exam_mode', function(e) {
    //     mode_select(2);
    // })      

    $(document).on('focus', '#btnSignOut', function(e) {
        responsiveVoice.speak('您選擇的是登出' , "Chinese Female");
    })

    $('.btn_click').on('focus', function() {
        responsiveVoice.speak('您選擇的是' + $(this).text() , "Chinese Female");
    })

    $(".buttton_click").on('focus', function() {
        responsiveVoice.speak('您選擇的是' + $(this).text() , "Chinese Female");
    });

    $('#btnMode').on('focus', function() {
        responsiveVoice.speak('您選擇的是' + $(this).text() , "Chinese Female");
    })

    // $(document).on('keydown', function(e) {
    //     console.log('keyCode: '+ e.keyCode);
    //     if(e.which == 84) {
    //         responsiveVoice.speak('請選擇模式, 您可以使用方向鍵進行選擇', "Chinese Female");
    //     }

    //     if(e.which == 39 || e.which == 40) {
    //         if(!modeSelected) {
    //             if(modeIndex < modeList.length - 1) modeIndex++;
    //             else modeIndex = modeList.length - 1;

    //             mode_select(modeIndex);
    //         }
    //         else {
    //             if(topicIndex < topicList.length - 1) topicIndex++;
    //             else topicIndex = topicList.length - 1;

    //             topic_select(topicIndex);
    //         }
    //     }

    //     if(e.which == 37 || e.which == 38 && !modeSelected) {
    //         if(!modeSelected) {
    //             if(modeIndex > 0) modeIndex--;
    //             else modeIndex = 0;

    //             mode_select(modeIndex);
    //         }
    //         else {
    //             if(topicIndex > 0) topicIndex--;
    //             else topicIndex = 0;

    //             topic_select(topicIndex);
                
    //         }
    //     }

    //     if(e.which == 13 && modeIndex != -1) {
    //         modeSelected = true;
    //         mode_enter(modeIndex);
    //         responsiveVoice.speak('請選擇題組, 您可以使用方向鍵進行選擇', "Chinese Female");
    //     }

    //     if(e.which == 27 && modeSelected) {
    //         modeSelected = false;
    //         mode_enter();
    //         responsiveVoice.speak('請選擇模式, 您可以使用方向鍵進行選擇', "Chinese Female");
    //     }
    // })


    function user_signOut() {
        window.location.href = '/logout';
    }

    function mode_select(id) {
        
        let mode = modeList[id];
        responsiveVoice.speak('您選擇的是' + mode, "Chinese Female");
    }

    function mode_enter(id) {
        if(id == undefined) {
            $('#divMode').show();
            $('#divTopic').hide();
            $('#btnMode').hide();
            $('#lblMode').hide();
            return;
        }

        let mode = modeList[id];
        responsiveVoice.speak('您選擇的是' + mode, "Chinese Female");
        modeIndex = id;

        $('#divMode').hide();
        $('#divTopic').show();
        $('#btnMode').show();
        $('#lblMode').show();
        $('#lblMode').text(mode);
    }

    function topic_select(id) {
        
        let topic = topicList[id];
        responsiveVoice.speak('您選擇的是' + topic, "Chinese Female");
        topicIndex = id + 1;
        
        switch(modeIndex) {
            case 0:
                window.location.href = '/topic/learn?tid='+ topicIndex + '&order=1';
                break;
            case 1:
                window.location.href = '/topic/train?tid='+ topicIndex + '&order=1';
                break;
            case 2:
                window.location.href = '/topic/exam?tid='+ topicIndex + '&order=1';
                break;

        }
    }



</script>

<style>
    .block {
        border: 1px solid #808080;

    }

    .block-select {
        border: 1px solid #808080;
    }
</style>

@endsection