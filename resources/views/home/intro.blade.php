@extends('layouts.app')

@section('title', '注意事項')

@section('content')

<script type="text/javascript">
    $(function() {
    
        // responsiveVoice.stop();

        // if(user_valid()) return;

        introduce();
    });
        
        $(document).on('keypress', function(e) {
            console.log(e);
    
            if(e.code === 'KeyQ' && e.ctrlKey) { 
                // console.log('page_back');
                window.location.href = '/';
            }
        });
    
       
        // function user_valid() {
        //     let user = sessionStorage.getItem('user');
    
        //     if(user) {
        //         window.location.href = '/home';
        //         return true;
        //     }
    
        //     return false;
        // }
        
        function user_signUp() {
            
            window.location.href = '/sign-up';
        }

        responsiveVoice.enableWindowClickHook();
        responsiveVoice.clickEvent();
        responsiveVoice.setDefaultVoice("Chinese Female");

        function introduce() {
            
            $('#btnSkip').focus();
    
            responsiveVoice.speak('親愛的老師學生及家長們非常歡迎您光臨聰明省眼力英語小學堂平台', "Chinese Female", {onend: AppluseCallback});
            
        }

        function AppluseCallback(){
           
            $('#divText').hide();
            $('#divApplause').show();
            
            // sound.play('./sounds/applause.mp3');
            let audio = new Audio('./sounds/applause.mp3');
            audio.play();
            setTimeout('AppluseCallback2()', 3000);
        }

        function AppluseCallback2(){
            $('#divText').show();
            $('#divApplause').hide();
            responsiveVoice.speak('同學們，好的開始就是成功的一半，當你決定並剛進入使用這個學習平台，基本上就已經站在邁向比其他人更前面一點起跑線上了，恭喜你', "Chinese Female", {onend: CheerCallback});
        }

        function CheerCallback(){

            $('#divText').hide();
            $('#divCheer').show();
            let audio = new Audio('./sounds/cheer.mp3');
            audio.play();
            setTimeout('CheerCallback2()', 3000);
        }

        function CheerCallback2(){
            $('#divText').show();
            $('#divCheer').hide();            
            responsiveVoice.speak('希望你在每一次登入都能夠更進步 更開心的學習 更沒有壓力練習及更一步一腳印的更上一層樓喔，請按確認開始填寫註冊資料', "Chinese Female", {onend: SignUp});
        }

        function SignUp(){
            $('#btnSignUp').focus();
        }
      
</script>

<div class="col-sm-11 mx-auto shadow-lg px-5 py-4 bg-light rounded-lg">
    <div id="divText" class="w-100">
        <h1>親愛的老師、學生及家長們 非常歡迎您光臨「聰明省眼力英語小學堂平台」</h1>
        <h1>同學們，好的開始就是成功的一半，當你決定並剛進入使用這個學習平台，
            基本上就已經站在邁向比其他人更前面一點起跑線上了！恭喜你！</h1>
        <h1>希望你在每一次登入都能夠更進步、更開心的學習、更沒有壓力練習及更一步一腳印的更上一層樓喔！</h1>
        <h1>首次進入使用者本平台需註冊，建立帳戶即表示您同意本平台的
            <a href="#" id="btnPrivacy" data-toggle="modal" data-target="#modalPrivacy">使用者條款與條件</a>
        </h1>
        <div class="col-sm-4 mt-5 mx-auto">
            <button type="button" id="btnSignUp" class="btn btn-primary btn-block btn-lg rounded-pill"
                onclick="user_signUp()">確認</button>
        </div>
    </div>
    <div id="divApplause" class="text-center" style="display:none;">
        <img src="{{ asset('img/applause.gif') }}" alt="applause" class=""
            style="width: 100%; height: auto !important" />
    </div>
    <div id="divCheer" class="text-center" style="display:none;">
        <img src="{{ asset('img/cheer.gif') }}" alt="cheer" class="" style="width: 100%; height: auto !important" />
    </div>
</div>



<div class="modal fade" id="modalPrivacy" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">隱私政策</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="my-2">
                    親愛的老師、學生及家長們
                    非常歡迎您光臨「聰明省眼力英語小學堂平台」(以下簡稱本平台)，為了讓您能夠安心使用本平台的各項服務與資訊，特此向您說明本平台的隱私權保護及使用權限政策。為保障您的權益，請您詳閱下列內容:
                </h4>
                <h5>一、隱私權保護政策的適用範圍</h5>
                <label class="ml-1">
                    隱私權保護政策內容，包括本平台如何處理在您使用網頁服務時收集到的個人識別資料。隱私權保護政策不適用於本平台以外的相關連結網站，也不適用於非本平台所委託或參與管理的人員。
                </label>
                <h5>二、個人資料的蒐集、處理及利用方式</h5>
                <label class="ml-1">
                    • 當您造訪本平台或使用本平台網站所提供之功能服務時，我們將視該服務功能性質，請您提供必要的個人資料，並在該特定
                    目的範圍內處理及利用您的個人資料;非經您書面同意，本平台不會將個人資料用於進一步的其他用途。
                </label>
                <label class="ml-1">
                    • 本學習平台在您使用服務信箱、問卷調查等互動性功能時，會妥善處理您所提供各項資料，如: 姓名、健康狀況、電子郵件地
                    址、聯絡方式等及個人資料，並保護您的個人穩私。
                </label>
                <label class="ml-1">
                    • 於一般瀏覽時，伺服器會自行記錄相關行徑，包括您使用連線設備的 IP 位址、使用時間、使用的瀏覽器、瀏覽及點選資料
                    記錄等，做為我們增進網站服務的參考依據，此記錄為內部應用及平台改善依據。
                </label>
                <label class="ml-1">
                    • 為提供精確的服務，我們會將收集的問卷調查內容進行統計與分析，分析結果之統計數據或說明文字呈現，除供內部及以學
                    術研究為主要目的之研究外，我們會視需要公佈統計數據及說明文字，但不涉及特定個人之資料。
                </label>
                <h5>三、資料之保護</h5>
                <label class="ml-1">
                    •
                    本平台網站主機均設有防火牆、防毒系統等相關的各項資訊安全設備及必要的安全防護措施，加以保護網站及您的個人資料採用嚴格的保護措施，只由經過授權的人員才能接觸您的個人資料，相關處理人員皆簽有保密合約，如有違反保密義務者，將會受到相關的法律處分。
                </label>
                <label class="ml-1">
                    • 如因業務需要有必要委託其他單位提供服務時，本網站亦會嚴格要求其遵守保密義務，並且採取必要檢查程序以確定其將確實遵守。
                </label>
                <h5>四、網站對外的相關連結</h5>
                <label class="ml-1">
                    本平台的網頁提供其他網站的網路連結，您也可經由本平台所提供的連結，點選進入其他網站。但該連結網站不適用本網站的隱私權保護政策，您必須參考該連結網站中的隱私權保護政策。
                </label>
                <h5>五、與第三人共用個人資料之政策</h5>
                <label class="ml-1">
                    本平台絕不會提供、交換、出租或出售任何您的個人資料給其他個人、團體、私人企業或公務機關，但有法律依據或合約義務者，不在此限。
                    前項但書之情形包括不限於:
                </label>
                <label class="ml-3">• 經由您書面同意。</label><br>
                <label class="ml-3">• 法律明文規定。</label><br>
                <label class="ml-3">• 為免除您生命、身體、自由或財產上之危險。</label><br>
                <label class="ml-3">
                    • 與公務機關或學術研究機構合作，基於公共利益為統計或學術研究而有必要，且資料經過提供者處理或蒐集著依其揭露方式無從識別特定之當事人。
                </label>
                <label class="ml-3">
                    • 當您在網站的行為，違反服務條款或可能損害或妨礙網站與其他使用者權益或導致任何人遭受損害時，經網站管理單位研析揭露您的個人資料是為了辨識、聯絡或採取法律行動所必要者。
                </label>
                <label class="ml-3">• 有利於您的權益。</label>
                <label class="ml-3">
                    • 本網站委託廠商協助蒐集、處理或利用您的個人資料時，將對委外廠商或個人善盡監督管理之責。
                </label>
                <h5>六、Cookie 之使用</h5>
                <label class="ml-1">
                    為了提供您最佳的服務，本平台網站會在您的電腦中放置並取用我們的 Cookie，若您不願接受 Cookie 的寫入，您可在您使用的瀏覽器功能項中設定隱私權等級為高，即可拒絕 Cookie
                    的寫入，但可能會導致網站某些功能無法正常執行 。
                </label>
                <h5>七、隱私權保護政策之修正</h5>
                <label class="ml-1">本網站隱私權保護政策將因應需求隨時進行修正，修正後的條款將刊登於網站上。</label>
                <h5 class="mt-2">智慧財產權聲明</h5>
                <label class="ml-1">
                    本平台並未授權使用者或任何人利用任何可能由本平台及網頁取得的機密資訊所衍生之中華民國或其他國家之專利、著作權或專業技術。使用者目前只限為合作學校校方老師、學生及其家長，且首次註冊使用本平台需經本平台管理者授權同意，未經授權註冊不得私自使用本平台。
                    本平台、網站中所有相關介面、功能及服務之所有權及智慧財產權皆保留，未經正式書面授權同意請勿隨意揭露公開散播。本平台目前為測試版本。
                </label>
            </div>
        </div>
    </div>
</div>

@endsection