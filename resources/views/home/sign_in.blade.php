@extends('layouts.app')

@section('title', '登 入')

@section('content')
<div class="container">
    <form id="app" name="app">
        <div class="col-sm-6 my-5 mx-auto shadow-lg px-5 py-3 bg-light rounded-lg">
            <div class="w-100">

                <h4 class="text-center mt-3 mb-5">登入</h4>
                <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text">帳號</span>
                    </div>
                    <input type="text" id="account" name="account" class="form-control" placeholder="">
                </div>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text">密碼</span>
                    </div>
                    <input type="password" id="password" name="password" class="form-control" placeholder="">
                </div>
                <label id="lblErrorSignin" class="text-danger"></label>
                <div class="text-center my-3">
                    <button type="button" id="btnSignin" class="btn btn-outline-primary rounded-pill mr-1"
                        onclick="user_signIn()">登入</button>
                    <button type="button" id="btnBack" class="btn btn-outline-secondary rounded-pill"
                        onclick="page_back()">返回</button>
                </div>
            </div>
        </div>
    </form>
</div>


<script type="text/javascript">
    $(function() {
    
        // responsiveVoice.stop();
        responsiveVoice.setDefaultVoice("Chinese Female");

        // if(user_valid()) return;

        responsiveVoice.speak('使用者登入', "Chinese Female");
        $('#account').focus();
        
    });
        
        responsiveVoice.enableWindowClickHook();
        responsiveVoice.clickEvent();
        responsiveVoice.setDefaultVoice("Chinese Female");

        $(document).on('keypress', function(e) {
            console.log(e);
    
            if(e.code === 'KeyQ' && e.ctrlKey) { 
                // console.log('page_back');
                window.location.href = '/';
            }
        });
    
        $(document).on('keydown', '#account', function(e) {
            if(e.which != 13) {
                $('#lblError').text('');
                return;
            }
    
            if(!$('#account').val().trim()) {
                $('#account').focus();
            }
            else {
                $('#password').focus();
                // responsiveVoice.speak('請輸入登入密碼, 輸入完請按確認鍵');
            }
        });
    
        $(document).on('keydown', '#password', function(e) {
            if(e.which != 13) return;
    
            if(!$('#password').val().trim()) {
                $('#password').focus();
            }
            else {
                $('#btnSignin').click();
            }
        });
    
        $(document).on('focus', '#account', function(e) {
            responsiveVoice.speak('請輸入登入帳號 輸入完請按確認鍵'), "Chinese Female";
        });
    
        $(document).on('focus', '#password', function(e) {
            responsiveVoice.speak('請輸入登入密碼, 輸入完請按確認鍵', "Chinese Female");
        });
        
        $(document).on('focus', '#btnSignin', function(e) {
            responsiveVoice.speak('登入', "Chinese Female");
        });

        $(document).on('focus', '#btnBack', function(e) {
            responsiveVoice.speak('返回', "Chinese Female");
        });

        function user_valid() {
            let user = sessionStorage.getItem('user');
    
            if(user) {
                window.location.href = '/home';
                return true;
            }
    
            return false;
        }
    
        function user_signIn() {
    
            let account = $('#account').val();
            let password = $('#password').val();
    
            
            $.ajax({
                url: '/user/sign-in',
                type: 'POST',
                data: $('#app').serialize(),
                error: function(err) {
                    console.log(err)
                    // console.log('Ajax Request Error');
                },
                success: function(res) {
                    console.log(res);
                    if(res == 'failed') {
                        responsiveVoice.speak('帳號登入失敗, 請重新輸入帳號', "Chinese Female");
                        $('#lblError').text('帳號密碼錯誤!');
                        $('#account').val('');
                        $('#password').val('');
                        $('#account').focus();
                    } else {
                        // console.log(resData.content);
                        // let name = JSON.parse(resData.content.profile).student_name;
                        responsiveVoice.speak('帳號登入成功 '+ account +' 歡迎你回來', "Chinese Female");
                        // sessionStorage.setItem('user', JSON.stringify(resData.content));
                        window.location.href = '/topic';
                    }
                }
            })
           
        }
         
</script>
@endsection