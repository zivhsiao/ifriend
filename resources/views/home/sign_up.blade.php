@extends('layouts.app')

@section('title', '註 冊')

@section('content')
<div class="container">
    <form method="post" name="app" id="app">
        <div class="col-sm-12 mx-auto shadow-lg px-5 py-3 bg-light rounded-lg">
            <div class="w-100">
                <h4 class="text-center mt-3 mb-4">使用者
                    註冊</h4>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text text-center">帳號</span>
                    </div>
                    <input type="text" name="account" id="account" class="form-control" placeholder="">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">密碼</span>
                    </div>
                    <input type="password" name="password" id="password" class="form-control" placeholder="">
                </div>
                <!-- <div id="divPassword" class="input-group mb-3" style="display:none;"> -->
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">確認密碼</span>
                    </div>
                    <input type="password" name="confirm_password" id="confirm_password" class="form-control"
                        placeholder="">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">姓名</span>
                    </div>
                    <input type="text" name="name" id="name" class="form-control" placeholder="">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">學校</span>
                    </div>
                    <input type="text" name="school" id="school" class="form-control" placeholder="">
                </div>
                <div class="row">
                    <div class="col-sm-6 input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">年級</span>
                        </div>
                        <input type="text" name="grade" id="grade" class="form-control text-center" placeholder="">
                        <div class="input-group-append">
                            <span class="input-group-text">年</span>
                        </div>
                    </div>
                    <div class="col-sm-6 input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">班級</span>
                        </div>
                        <input type="text" name="class" id="class" class="form-control text-center" placeholder="">
                        <div class="input-group-append">
                            <span class="input-group-text">班</span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend mx-auto">
                        <span class="input-group-text">出生日期</span>
                    </div>
                    <input data-date-format="yyyy-mm-dd" name="birthday" id="birthday" class="form-control" readonly
                        placeholder="">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">家長姓名</span>
                    </div>
                    <input type="text" name="parent_name" id="parent_name" class="form-control" placeholder="">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">家長電話</span>
                    </div>
                    <input type="text" name="phone" id="phone" class="form-control" placeholder="">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">LINE ID</span>
                    </div>
                    <input type="text" name="line" id="line" class="form-control" placeholder="">
                </div>
                <!-- <div class="row ml-4 mb-3">
                        <input type="checkbox" id="chkPrivacy" class="form-check-input">
                        我同意網站
                        <a href="#" id="btnPrivacy" data-toggle="modal" data-target="#modalPrivacy">隱私政策</a>
                    </div> -->
                <label id="lblError" class="text-danger"></label>
                <div class="mt-4 mb-3 text-right">
                    <button type="button" id="btnSignup" class="btn btn-outline-primary rounded-pill mr-1"
                        onclick="signUp()">送出</button>
                    <button type="button" class="btn btn-outline-danger rounded-pill" onclick="page_back()">返回</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center">
                <span>註冊成功</span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
    
        // responsiveVoice.stop();

        // if(user_valid()) return;
        responsiveVoice.setDefaultVoice("Chinese Female");
        responsiveVoice.speak('使用者註冊', "Chinese Female");
        $('#account').focus();
        

        $('#birthday').datepicker({
            uiLibrary: 'bootstrap4'
        })

    });
        responsiveVoice.enableWindowClickHook();
        responsiveVoice.clickEvent();
        responsiveVoice.setDefaultVoice("Chinese Female");
    
        // $(document).on('keypress', function(e) {
        //     console.log(e);
    
        //     if(e.code === 'KeyQ' && e.ctrlKey) { 
        //         // console.log('page_back');
        //         window.location.href = '/';
        //     }
        // });
    
        // $(document).on('blur', '#txtPassword_1', function(e) {
        //     // // console.log($(this).val());
        //     // if($(this).val().trim() != '') {
        //     //     $('#divPassword').slideDown('0', function() {
        //     //         $('#txtPassword_2').val('');
        //     //         $('#txtPassword_2').focus();
        //     //     });
        //     //     // $('#divPassword').show();
        //     // }
        // });
    
        $(document).on('hidden.bs.modal', '#modalSuccess', function(e) {
            
            window.location.href = '/';
        })
    
    
       
    
        function signUp() {
            var data_valid = true;
    
            $("#app input").each(function(){
                // console.log($(this));
                if($(this).val().trim() == '') {
                    $(this).addClass('border-danger');
                    data_valid = false;
                }
            });
    
            if(!data_valid) {
                $('#lblError').text('註冊資料不完整!');
                // responsiveVoice.speak('註冊資料不完整');
                return;
            }
    
            // if(!$('#chkPrivacy').is(":checked")) {
            //     $('#lblError').text('請同意隱私政策!');
            //     return;
            // }
    
            if($('#password').val() != $('#confirm_password').val()) {
                $('#password').addClass('border-danger');
                $('#confirm_password').addClass('border-danger');
                $('#password').focus();
                $('#lblError').text('兩次輸入的密碼不一致!');
                return;
            }
    
            // console.log($('#app').serialize());
            
            $.ajax({
                url: '/user/sign-up',
                type: 'POST',
                data: $('#app').serialize(),
                error: function(err) {
                    console.log(err)
                    // console.log('Ajax Request Error');
                },
                success: function(res) {
                    // console.log(resData);
                    if(res == 'false') {
                        
                        $('#lblError').text('帳號重複!');
                        $('#account').val('');
                        $('#account').focus();
                    
                        return;
                    }
                   
                    $('#modalSuccess').modal('show');
                }
            })
           
    
        }
    
        function user_valid() {
            // let user = sessionStorage.getItem('user');
    
            if(user) {
                window.location.href = '/home';
                return true;
            }
    
            return false;
        }        
       
</script>
@endsection