@extends('layouts.app')

@section('title', '註 冊')

@section('content')
<div class="container-fluid">
    <div class="row my-5">
        <button id="menuBack" class="btn btn-success col-sm col-12 mr-sm-2 py-3 mb-3 font-size-40" style="height:25pc;">

            Ctrl鍵 + Q
            <hr>退出 / 回到上一頁
        </button>
        <button id="menuReplay" class="btn btn-success col-sm col-12 mr-sm-2 py-3 mb-3 font-size-40"
            style="height:25pc;">

            Ctr鍵 + R
            <hr>語音提示重播
        </button>
        <button id="menu" class="btn btn-success col-sm col-12 mr-sm-2 py-3 mb-3 font-size-40" style="height:25pc;">

            <hr>
        </button>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        responsiveVoice.clickEvent();
        responsiveVoice.setDefaultVoice("Chinese Female");
        responsiveVoice.speak('使用說明', "Chinese Female");
        
        $('#menuBack').focus();
    
    });
    
    responsiveVoice.setDefaultVoice("Chinese Female");

    $(document).on('keypress', function(e) {
        console.log(e);

        if(e.code === 'KeyQ' && e.ctrlKey) { 
            // console.log('page_back');
            window.location.href = '/';
        }else if(e.code === 'KeyR' && e.ctrlKey) { 
            // console.log('page_back');
            window.location.href = '/';
        }
    });

    $(document).on('focus', '#menuBack', function(e) {

        responsiveVoice.speak('退出或回到上一頁 使用是 Ctrl鍵 + Q', "Chinese Female");
    })

    $(document).on('focus', '#menuReplay', function(e) {

        responsiveVoice.speak('語音提示重播 使用是 Ctrl 鍵 + R', "Chinese Female");
    })
    
   
    function user_valid() {
        let user = sessionStorage.getItem('user');

        if(user) {
            window.location.href = '/';
            return true;
        }

        return false;
    }
    
   
</script>

@endsection