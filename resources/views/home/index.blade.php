@extends('layouts.app')

@section('title', '首 頁')

@section('sidebar')
<a href="/">
    <img src="{{ asset('img/logo-4.jpg') }}" style="width: 100%; height: auto; margin: 0 auto">
</a>
@endsection

@section('content')
<script type="text/javascript">
    // window.onload = function() {
    //     if (!window.location.hash) {
    //         window.location = window.location + '#loaded';
    //         window.location.reload();
    //     }
    // }

    $(function() {

        // responsiveVoice.stop();
        
        // user_valid();
        responsiveVoice.enableWindowClickHook();
        responsiveVoice.clickEvent();
        responsiveVoice.setDefaultVoice("Chinese Female");

        responsiveVoice.speak("歡迎進入聰明省眼力英語小學堂，請選擇登入、註冊或說明", "Chinese Female");
            
        $('#menuSignIn').focus();    

        $(document).on('focus', '#menuSignIn', function(e) {
            responsiveVoice.speak('使用者登入', "Chinese Female");
        })

        $(document).on('focus', '#menuSignUp', function(e) {
            responsiveVoice.speak('使用者註冊', "Chinese Female");
        })

        $(document).on('focus', '#menuHelp', function(e) {
            responsiveVoice.speak('使用說明', "Chinese Female");
        })

        $(document).on('mouseover', '#menuSignIn', function(e) {
            responsiveVoice.speak('使用者登入', "Chinese Female");
        })

        $(document).on('mouseover', '#menuSignUp', function(e) {
            responsiveVoice.speak('使用者註冊', "Chinese Female");
        })

        $(document).on('mouseover', '#menuHelp', function(e) {
            responsiveVoice.speak('使用說明', "Chinese Female");
        })

    });
    
    

    
    
    function user_signIn() {
        window.location.href = '/sign-in';
    }

    function user_intro() {
        window.location.href = '/intro';
    }

    function user_help() {
        window.location.href = '/help';
    }

    // function user_welcome() {

    //     responsiveVoice.speak('歡迎進入聰明省眼力英語小學堂');

    //     $('#menuSignIn').focus();
    // }


    
</script>

<div class="container-fluid">
    <div class="row mt-3 pt-3 text-center">
        <button id="menuSignIn" class="btn col-sm col-12 my-2 mr-sm-3 font-size-60"
            style="height:250px; background-color: #64a600; color: white" onclick="user_signIn()">使用者登入</button>
        <button id="menuSignUp" class="btn btn-primary col-sm col-12 my-2 mx-sm-3 font-size-60" style="height:250px;"
            onclick="user_intro()">使用者註冊</button>
        <a id="menuHelp" class="btn col-sm col-12 my-2 ml-sm-3 font-size-60"
            style="height:250px; background-color: #ae00ae; color: white; text-align:center; line-height:230px"
            href="/pdf/EyeFriendly_User_Menual.pdf" target="_blank">使用說明</a>
    </div>
</div>



<div id="smart-button-container">
  <div style="text-align: center"><label for="description">贊助&捐款者: 如果您喜歡此學習平台, 歡迎給我們研究團隊一點鼓勵。請輸入您的贊助大名或匿名。 </label><input type="text" name="descriptionInput" id="description" maxlength="127" value=""></div>
    <p id="descriptionError" style="visibility: hidden; color:red; text-align: center;">Please enter a description</p>
  <div style="text-align: center"><label for="amount">金額: 數位多媒體科技輔助國小生英語學習-線上課程, 比賽及研究計劃贊助金額。 </label><input name="amountInput" type="number" id="amount" value="" ><span> TWD</span></div>
    <p id="priceLabelError" style="visibility: hidden; color:red; text-align: center;">Please enter a price</p>
  <div id="invoiceidDiv" style="text-align: center; display: none;"><label for="invoiceid">贊助對象, 可填入: 1)數位多媒體科技輔助國小生英語學習計劃。2)線上課程研發。 3)比賽活動及獎項。4)請研究人員喝咖啡。 </label><input name="invoiceid" maxlength="127" type="text" id="invoiceid" value="" ></div>
    <p id="invoiceidError" style="visibility: hidden; color:red; text-align: center;">Please enter an Invoice ID</p>
  <div style="text-align: center; margin-top: 0.625rem;" id="paypal-button-container"></div>
</div>
<script src="https://www.paypal.com/sdk/js?client-id=sb&currency=TWD" data-sdk-integration-source="button-factory"></script>
<script>
function initPayPalButton() {
  var description = document.querySelector('#smart-button-container #description');
  var amount = document.querySelector('#smart-button-container #amount');
  var descriptionError = document.querySelector('#smart-button-container #descriptionError');
  var priceError = document.querySelector('#smart-button-container #priceLabelError');
  var invoiceid = document.querySelector('#smart-button-container #invoiceid');
  var invoiceidError = document.querySelector('#smart-button-container #invoiceidError');
  var invoiceidDiv = document.querySelector('#smart-button-container #invoiceidDiv');

  var elArr = [description, amount];

  if (invoiceidDiv.firstChild.innerHTML.length > 1) {
    invoiceidDiv.style.display = "block";
  }

  var purchase_units = [];
  purchase_units[0] = {};
  purchase_units[0].amount = {};

  function validate(event) {
    return event.value.length > 0;
  }

  paypal.Buttons({
    style: {
      color: 'gold',
      shape: 'pill',
      label: 'paypal',
      layout: 'vertical',
      
    },

    onInit: function (data, actions) {
      actions.disable();

      if(invoiceidDiv.style.display === "block") {
        elArr.push(invoiceid);
      }

      elArr.forEach(function (item) {
        item.addEventListener('keyup', function (event) {
          var result = elArr.every(validate);
          if (result) {
            actions.enable();
          } else {
            actions.disable();
          }
        });
      });
    },

    onClick: function () {
      if (description.value.length < 1) {
        descriptionError.style.visibility = "visible";
      } else {
        descriptionError.style.visibility = "hidden";
      }

      if (amount.value.length < 1) {
        priceError.style.visibility = "visible";
      } else {
        priceError.style.visibility = "hidden";
      }

      if (invoiceid.value.length < 1 && invoiceidDiv.style.display === "block") {
        invoiceidError.style.visibility = "visible";
      } else {
        invoiceidError.style.visibility = "hidden";
      }

      purchase_units[0].description = description.value;
      purchase_units[0].amount.value = amount.value;

      if(invoiceid.value !== '') {
        purchase_units[0].invoice_id = invoiceid.value;
      }
    },

    createOrder: function (data, actions) {
      return actions.order.create({
        purchase_units: purchase_units,
      });
    },

    onApprove: function (data, actions) {
      return actions.order.capture().then(function (details) {
        alert('Transaction completed by ' + details.payer.name.given_name + '!');
      });
    },

    onError: function (err) {
      console.log(err);
    }
  }).render('#paypal-button-container');
}
initPayPalButton();
</script>


@endsection