<?php
 
// Cross-Origin Resource Sharing Header
header('Access-Control-Allow-Origin:*');

?>
<!DOCTYPE html>
<html>

<head>
    <title>@yield('title') - EyeFriendly-English</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}?t={{ time() }}" />

    <!-- Font-Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css"
        integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <!-- Font-Awesome -->
    <!-- <script src="https://kit.fontawesome.com/cd3547f5c3.js" crossorigin="anonymous"></script> -->

    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

    <script src="https://code.responsivevoice.org/responsivevoice.js?key=5VVEUcWL"></script>

</head>

<body>
    <div class="container text-center">
        @section('logo')
        <a href="/">
            <img src="{{ asset('img/logo-4.jpg') }}" style="width: 100%; height: auto; margin: 0 auto">
        </a>
        @show
    </div>
    <br>
    <script type="module">
        import * as voice from './js/voice/voice.js';
        import * as sound from './js/voice/sound.js';
    
        window.voice = voice;
        window.sound = sound;
    </script>

    <script>
        responsiveVoice.enableWindowClickHook();
        responsiveVoice.clickEvent();
        responsiveVoice.setDefaultVoice("Chinese Female");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function page_back() {
            window.location.href = '/';
        }

        function wait(ms)
        {
            var d = new Date();
            var d2 = null;
            do { d2 = new Date(); }
            while(d2-d < ms);
        }
    </script>

    @yield('content')
</body>

</html>