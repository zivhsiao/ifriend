@extends('layouts.app')

@section('title', '學習模式')

@section('logo')
<a href="/">
    <img src="{{ asset('img/logo-thum.jpg') }}" style="width: 100%; height: auto; margin: 0 auto">
</a>
@endsection

@section('content')
<script src="{{ asset('js/custom.js') }}?t={{ time() }}"></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-4">
            <button id="save_exit" type="button" class="btn btn-outline-primary btn-lg button_click"
                onclick="user_signOut()">離開並儲存</button>
            {{-- <input type="text" id="timer"> --}}
        </div>
        <div class="col-4">
            <label id="sys_title" class="font-size-40">學習模式</label>
        </div>
        <div class="col-4">
            <a id="quick_key_manual" type="button" class="btn btn-outline-primary btn-lg button_click"
                href="/pdf/EyeFriendly_User_Menual.pdf" target="_blank">使用說明</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col">
            <button style="display: none" id="pause" type="button" class="btn btn-outline-primary btn-lg button_click"
                onclick="time_pause()">暫停</button>
        </div>
        <div class="col">
            <label id="lblTopic" class="font-size-40">題組</label><br>
            <label id="lblTopicNum" class="font-size-20 lblTopicNum"></label>
        </div>
        <div id="timeClock" class="col">
            <span class="my-3 font-size-20">計時</span><br>
            <span class="my-3 font-size-20">本題時間：</span><span id="lblTime" class="my-3 font-size-20">0</span><span
                class="my-3 font-size-20"> 秒</span><br>
            <span class="my-3 font-size-20" style="display: none">總計時間：</span><span id="lblTimeAll"
                style="display: none" class="my-3 font-size-20">0</span><span class="my-3 font-size-20"
                style="display: none"> 秒</span>
            <input type="hidden" id="lblTimeAll_sec">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-8">
            <label id="lblNumber" class="my-3 font-size-40"> 第 <span id="num"></span> 題</label>
            <br>
            <img id="imgShow" style="display: none; width: 600px; height: 600px" src="" alt="">
        </div>
        <div class="col-4">
            <button id="text_display" type="button" class="btn btn-outline-primary btn-lg button_click"
                onclick="text_display()">顯示文字</button>
            <br>
            <label id="lblContent" class="my-3 font-size-40"></label>
        </div>
    </div>
    <br>
    <div id="speak_voice" class="row">
        <div id="eng_text_voice" class="col"></div>
        <div id="eng_text_2time_voice" class="col"></div>
        <div id="zh_text_voice" class="col"></div>
        <div id="zh_eng_voice" class="col"></div>
    </div>
    <br>
    <div class="row">
        <div class="col" id='record_voice' style="display: none">
            請跟我說<br>

            <span id="rec_text_01"></span> <input style="display: none" type="text" id="rec_01">
            <button class="button_rec"></button>
            <button id="rec_button_start_1" onclick="startRecording(this);">錄音開始</button>
            {{-- <button id="rec_button_stop_1" class="" onclick="stopRecording(this);" disabled>停止</button> --}}
            <span id="download_1"></span>
            <br>
            <span id="rec_text_02"></span> <input style="display: none" type="text" id="rec_02"">
            <button class=" button_rec2" style=""></button>
            <button id="rec_button_start_2" onclick="startRecording2(this);">錄音開始</button>
            {{-- <button id="rec_button_stop_2" class="" onclick="stopRecording2(this);" disabled>停止</button> --}}
            <span id="download_2"></span>
            <ul id="recordingslist" style="display: none;"></ul>
            <textarea id="log" cols="50" rows="8" style="display: none;"></textarea>
        </div>
        <div class="col" id="type_word" style="display:none;">
            請輸入 <span class="type_word"></span>：<input type="text" id="type_01"><br>
            請輸入 <span class="type_word"></span>：<input type="text" id="type_02"><br>
            請輸入 <span class="type_word"></span>：<input type="text" id="type_03"><br>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col">
            <button id="prev_page" type="button" data=""
                class="btn btn-outline-primary btn-lg button_click">上一題</button>
        </div>

        <div class="col text-right">
            <button id="next_page" type="button" data=""
                class="btn btn-outline-primary btn-lg button_click">下一題</button>

            <button id="view_transcript" type="button" data="" class="btn btn-outline-primary btn-lg button_click"
                style="display: none">觀看成績</button>
        </div>
    </div>
</div>

<div id="notify_show" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLongTitle">通知</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table style="width: 100%; font-size: 26px;">
                    <thead>
                        <tr>
                            <th>內容</th>
                            <th>時間</th>

                        </tr>
                    </thead>
                    <tbody id="notify_message">
                        <tr>
                            <td></td>
                            <td></td>

                        </tr>
                    </tbody>

                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">關 閉</button>

            </div>
        </div>
    </div>
</div>

<div id="exit_show" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLongTitle">通知</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>
                    小朋友, 提醒你, 請按任一鍵繼續喔!<br>
                    你也可按儲存並登出觀看今天的成績!<br>
                    若無動作，2 分鐘之後，系統將自動登出
                </h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">關 閉</button>

            </div>
        </div>
    </div>
</div>



<div id="finish_show" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">小朋友, 這是你這次的成績喔</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="finish_body">
                <h3>
                    此為本題組最後一題, 恭禧你完成了<br>

                </h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">關 閉</button>

            </div>
        </div>
    </div>
</div>

<embed id="media" src="" width="180" height="90" loop="false" autostart="true" hidden="true" />

<audio id="media_mp3" src="" controls autoplay hidden></audio>

<script type="text/javascript">
    responsiveVoice.clickEvent();
responsiveVoice.setDefaultVoice("Chinese Female");

$('.button_click').on('focus', function(){
    responsiveVoice.speak($(this).text(), "Chinese Female");
})

$('.button_rec').on('focus', function(){
    responsiveVoice.speak('請跟我說 ' + $('.type_word').html(), "Chinese Female");
    // responsiveVoice.speak($('.type_word').html(), "US English Female");
})

$('.button_rec2').on('focus', function(){
    responsiveVoice.speak('請說 ' + $('.type_word').html(), "Chinese Female");
    // responsiveVoice.speak($('.type_word').html(), "US English Female");
})

$('#type_01').on('focus', function(){
    responsiveVoice.speak('請輸入 ' + $('.type_word').html(), "Chinese Female");
    // responsiveVoice.speak($('.type_word').html(), "US English Female");
});

$('#type_02').on('focus', function(){
    responsiveVoice.speak('請輸入 ' + $('.type_word').html(), "Chinese Female");
    // responsiveVoice.speak($('.type_word').html(), "US English Female");
});

$('#type_03').on('focus', function(){
    responsiveVoice.speak('請輸入 ' + $('.type_word').html(), "Chinese Female");
    // responsiveVoice.speak($('.type_word').html(), "US English Female");
});
$('#type_01').on('keypress', function(e){
    if(e.which == 13) {
        $('#type_02').focus();
    }  
});

$('#type_02').on('keypress', function(e){
    if(e.which == 13) {
        $('#type_03').focus();
    }
});

$('#next_page').on('focus', function(e){
    responsiveVoice.speak('下一題', "Chinese Female");
})
$('#prev_page').on('focus', function(e){
    responsiveVoice.speak('上一題', "Chinese Female");
})

$('#type_03').on('keypress', function(e){
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order_id = new URLSearchParams(window.location.search).get('order');
    if(e.which == 13) {
        type_01 = $("#type_01").val();
        type_02 = $("#type_02").val();
        type_03 = $("#type_03").val();
        order_time = $('#lblTime').html();
        ttl_time = $('#lblTimeAll').html();
        let level = 1;

        save_to_server(topic_no, order_id, type_01, type_02, type_03, order_time, ttl_time, level);

        $.ajax({
            url: '/topic/request',
            type: 'GET',
            data: {
                'tid': topic_no,
                'order': order_id
            },
            // data: JSON.stringify(reqData),
            beforeSend: function() {
                
            },
            error: function(err) {
                console.log(err)
                // console.log('Ajax Request Error');
            },
            success: function(res) {
                console.log(res);
                window.location = 'learn?' + res.next_page + '&time=' + $('#lblTimeAll_sec').val();
            }
        })

        
    }
});



function check_audio(){
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');

    $('#download_1').attr('href', '/upload/audio/{{ Session::get('account_id') }}/' + topic_no + '/' + order + '/audio_1.mp3');
    $('#download_2').attr('href', '/upload/audio/{{ Session::get('account_id') }}/' + topic_no + '/' + order + '/audio_2.mp3');

    $.post('/ajax/check_audio/' + topic_no + '/' + order, function(res){

        if(res.status.audio_1 == true){
            $('#download_1').show();
        };
        if(res.status.audio_2 == true){
            $('#download_2').show();
        };
    })
}

</script>
<script src="{{ asset('js/recorder.js') }}"></script>

@endsection