let topic_no;
let question;
let question_index;
let question_break;

responsiveVoice.clickEvent();
responsiveVoice.setDefaultVoice("Chinese Female");

$(function() {

    // clearTimeout();
    
    // if(!user_valid()) return;
   
    // mode_enter();
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');

    // 寫入 Log
    $.post('/ajax/post_log', {
        'level': 1,
        'question_type': topic_no
    }, function(data) {

        // if(data.result == 1){
        //     if(data.order + 1 != order){
        //         if(data.last_order == 0){
        //             window.location = 'learn?tid=' + data.question_type + '&order=' + (data.order + 1);
        //         }
        //     }
            
        // }
    })


    // 題組
    $('#lblTopic').text('題組 - '+ topic_no);
    topic_get(topic_no, order);

    let ele_timer = document.getElementById("lblTime");
    let n_sec = 0; //秒
    let n_min = 0; //分
    let n_hour = 0; //時//60秒 === 1分//60分 === 1小時
    let onTimerEnd = 0;

    // 計時用
    function timer() { 
        return setInterval(function () { 
            let str_sec = n_sec; 
            let str_min = n_min; 
            let str_hour = n_hour; 
            if ( n_sec < 10) { 
                str_sec = "0" + n_sec; 
            } 
            if ( n_min < 10 ) {
                str_min = "0" + n_min; 
            } 
            if ( n_hour < 10 ) {
                str_hour = "0" + n_hour; 
            } 
            let time = str_hour + ":" + str_min + ":" + str_sec;
            // console.log(time);
            $('#lblTime').html(time);
            n_sec++; 
            if (n_sec > 59){ 
                n_sec = 0; n_min++; 
            } 
            if (n_min > 59) { 
                n_sec = 0; 
                n_hour++; 
            } 
            onTimerEnd += 1000;
            
        }, 1000);
    }

    let n_timer = timer();

    function ReCalculate(){
        clearTimeout(onTimerEnd);
        onTimerEnd = setTimeout(function(){
            Timeout();
        }, 60 * 6 * 1000); //js 是用毫秒計算
        
    }
    
    document.onkeydown = ReCalculate;
    // // document.onmousemove = ReCalculate;
    ReCalculate();  

    function Timeout(){
        $('#media').attr('src', '/sound/notify2.wav');
        $('#media_mp3').attr("src", '/sounds/notify2.wav');
        responsiveVoice.speak('小朋友, 提醒你, 請按任一鍵繼續喔,你也可按儲存並登出觀看今天的成績,若無動作，2 分鐘之後，系統將自動登出', "Chinese Female");
        // alert("\n您好\n您可能已離開!\n很抱歉!!請重新登入系統\n謝謝!!");
        $('#exit_show').modal('show');
        // location.href= ('/logout');
        Timeout2();
        
    }
    
    function Timeout2(){
    
        let level = 1;

        $.post('/ajax/delete_server', 
        {
            'level': level
        }, function(e){

        })

        setTimeout(function(){
            user_signOut();
        }, 120 * 1000)
    }


    // 觀看成績
    $('#view_transcript').on('click', function(){
        view_transcript();
    })

});


function view_transcript(){
    let topic_no = new URLSearchParams(window.location.search).get('tid');
        
        // created_at: null
        // deleted_at: null
        // en_read_1: "a.m4a"
        // en_read_2: "a-a_a-a.m4a"
        // en_read_3: "a."
        // en_title: "a"
        // enabled: 1
        // id: 2004
        // row_1: None: -1
        // order: 1
        // picture: null
        // question_type: "1"
        // read_en: "a"
        // read_zh: "一個(張、隻...等)，子音之前"
        // sound: null
        // speech_01: null
        // speech_02: null
        // speech_message_01: "error"
        // speech_message_02: "error"
        // type_01: "a"
        // type_02: "a"
        // type_03: "a"
        // type_message_01: ""
        // type_message_02: ""
        // type_message_03: ""
        // zh_title: "一個(張、隻...等)，子音之前"
        // en_title1: 答案
        
        
        let text_html = '';
        let all_score = 0;
        $.post('/ajax/all_group', {
            'level': 1,
            'question_type': topic_no
        }, function(data){
            text_html = '請檢查以下<br>';
            // console.log(data)
            for(let x = 0; x < data.length; x++){
                if(data[x].order_1 != -1){
                    if(data[x].type_message_01 == 'error' || data[x].type_message_02 == 'error' && 
                        data[x].speech_message_01 == 'error' || data[x].speech_message_02 == 'error' || 
                        data[x].speech_message_03 == 'error'){

                            all_score += data[x].all_score;
                        
                            text_html += '問題' + data[x].order +'：' + data[x].en_title ;
                            
                            if(data[x].type_message_01 == 'error'){
                                data[x].type_01 = '未輸入';
                            }    
                            text_html += '，打字 1：' + data[x].type_01;
                            
                            if(data[x].type_message_02 == 'error'){
                                data[x].type_02 = '未輸入';
                            }
                            text_html += '，打字 2：' + data[x].type_02;
                            
                            if(data[x].type_message_03 == 'error'){
                                data[x].type_03 = '未輸入';
                            }
                            text_html += '，打字 3：' + data[x].type_03;
                            
                            if(data[x].speech_message_01 == 'error' ){
                                data[x].speech_01 = '未輸入';
                            }
                            text_html += '，錄音 1：' + data[x].speech_01;
                            
                            if(data[x].speech_message_02 == 'error' ){
                                data[x].speech_02 = '未輸入';
                            }
                            text_html += '，錄音 2：' + data[x].speech_02;
                            

                            text_html += '<br>';
                    }
                }
                
            }

            text_html += '總分：' + all_score.toFixed(2);

            $('#finish_body').html(text_html);
            responsiveVoice.speak('小朋友, 這是你這次的成績喔，' + $('#finish_body').text(), "Chinese Female");
            $('#finish_show').modal('show');
        })
}

// 按下按鍵要呈現的模式
$(document).on('keypress', function(e) {
    console.log(e);

    // 回選擇頁面
    if(e.code === 'KeyQ' && e.ctrlKey) { 
        // console.log('page_back');
        window.location.href = '/topic';
    }

    // Shift key + c.code Degit1
    if(e.shiftKey  && e.code == 'KeyS'){
        console.log(e.code);
        $('#rec_button_start_1').trigger('click');
    }

    if(e.shiftKey  && e.code == 'KeyD'){
        console.log(e.code);
        $('#rec_button_start_2').trigger('click');
    }

    if(e.shiftKey  && e.code == 'KeyH'){
        console.log(e.code);
        responsiveVoice.pause();
    }
    
    if(e.shiftKey  && e.code == 'KeyF'){
        console.log(e.code);
        responsiveVoice.resume();
    }

});



// 檢查 audio (可能用不到)

// 登出離開用
function user_signOut() {
    $('#exit_show').modal('hide');
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order_id = new URLSearchParams(window.location.search).get('order');
    let type_01 = $('#type_01').val();
    let type_02 = $('#type_02').val();
    let type_03 = $('#type_03').val();
    let order_time = $('#lblTime').html();
    let ttl_time = $('#lblTimeAll').html();
    let level = 1;
    save_to_server(topic_no, order_id, type_01, type_02, type_03, order_time, ttl_time, level);
        
    $.post('/ajax/delete_server', 
    {
        'level': level
    }, function(e){

        view_transcript();

        $('#finish_show').on('hidden.bs.modal', function () {
            window.location.href = '/logout';  
        });  
        
    })

}

function user_help() {
    window.location.href = "/pdf/EyeFriendly_User_Menual.pdf"
}


let delay_prev = function(s) {
    return new Promise(function(resolve, reject){
        setTimeout(resolve, s)
    })
}

let delay_next = function(s) {
    return new Promise(function(resolve, reject){
        setTimeout(resolve, s)
    })
}

function topic_get(topic_no, order_id) {

    $.ajax({
        url: '/topic/request',
        type: 'GET',
        data: {
            'tid': topic_no,
            'order': order_id
        },
        // data: JSON.stringify(reqData),
        beforeSend: function() {
            
        },
        error: function(err) {
            console.log(err)
            // console.log('Ajax Request Error');
        },
        success: function(res) {
            console.log(res);

            let en_title = res[0].en_title;
            let en_title2 = res[0].en_title2;
            let zh_title = res[0].zh_title;
            let read_en = res[0].read_en;
            let level = 1;

            en_arr = en_title.split("");
            en_title_join = en_arr.join(',');
            en_title_str = en_arr.join(" ");
            
            if(en_title2){
                en_arr2 = en_title2.split("");
                en_title_join2 = en_arr2.join(',');
                en_title_str2 = en_arr2.join(" ");
            }

            $('#lblTopicNum').html(res[0].order + '/' + res.count);

            $('#lblContent').hide();

            $('#num').html(res[0].order);

            if(res[0].order == 1){
                $.post('/ajax/delete_server', 
                {
                    'level': level
                }, function(e){

                })
            }

            if(res.prev_page != ''){
                $('#prev_page').on('click', function() {
                    // $(this).prop('data', 'learn?' + res.prev_page + '&time=' + $('#lblTimeAll').html());
                    
                    let type_01 = $('#type_01').val();
                    let type_02 = $('#type_02').val();
                    let type_03 = $('#type_03').val();
                    let order_time = $('#lblTime').html();
                    let ttl_time = $('#lblTimeAll').html();

                    delay_prev().then(function() {
                        save_to_server(topic_no, order_id, type_01, type_02, type_03, order_time, ttl_time, level);
                        
                        return delay_prev(1500);
                    }).then(function(){
                        window.location = 'learn?' + res.prev_page + '&time=' + $('#lblTimeAll_sec').val();
                    })

                })
            }
            
            if(res.next_page != ''){
                $('#next_page').on('click', function() {
                    // $(this).prop('data', 'learn?' + res.prev_page + '&time=' + $('#lblTimeAll').html());
                    let type_01 = $('#type_01').val();
                    let type_02 = $('#type_02').val();
                    let type_03 = $('#type_03').val();
                    let order_time = $('#lblTime').html();
                    let ttl_time = $('#lblTimeAll').html();
                    
                    delay_next().then(function() {
                        save_to_server(topic_no, order_id, type_01, type_02, type_03, order_time, ttl_time, level);
                        
                        return delay_next(1500);
                    }).then(function(){
                        window.location = 'learn?' + res.next_page + '&time=' + $('#lblTimeAll_sec').val();
                    })
                })
            } 

            if(res[0].en_title != null){
                $('#lblContent').html(res[0].en_title)
            }

            if(res[0].order == 1){
                responsiveVoice.speak('學習模式', "Chinese Female", {onend: speak2(res)})   
                
            } else {
                
                delay_speak(res);

            }                


            let topic_num = $('#lblTopicNum').html();
            let topic_num_arr = topic_num.split('/');
            if(topic_num_arr[0] == topic_num_arr[1]){
                $('#next_page').hide();
                $('#view_transcript').show();
            }
        }
    })
}

function save_to_server(){
    let args = Array.from(arguments);

    // console.log(args[0]);

    $.post('/ajax/save_server', {
        'topic': args[0],
        'order': args[1],
        'type_01': args[2],
        'type_02': args[3],
        'type_03': args[4],
        'order_time': args[5],
        'ttl_time': args[6],
        'level': args[7]
    }, function(e){
        // console.log(e);
        
    });
}

function delay_speak(res){
    // console.log(res);
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order_id = new URLSearchParams(window.location.search).get('order');

    let en_title = res[0].en_title;
    let en_title2 = res[0].en_title2;
    let zh_title = res[0].zh_title;
    let read_en = res[0].read_en;

    en_title = en_title.trim();

    if(en_title2){
        en_arr = en_title2.split("");    
    } else {
        en_arr = en_title.split("");
    }
    
    en_title_join = en_arr.join(',');
    en_title_str = en_arr.join(" ");
    read_en = read_en;
    sound = res[0].sound;
    sound = $.trim(sound);
    console.log(res[0].sound)
    delay().then(function(){
        if(res[0].sound != null){
            // $('#media').attr("src", '/sounds/' +sound + '#t=,2.5');
            $('#media_mp3').attr("src", '/sounds/' + sound + '#t=,2.5');
        }     
        return delay(1000);
    }).then(function(){
        if(res[0].picture != null){

            picture_url = res[0].picture;
            pic_arr = picture_url.split('://');

            if(pic_arr[0] == 'http'){
                file_arr = pic_arr[1];
                file_arr_sting = file_arr.split('/');
                file_string = file_arr_sting[file_arr_sting.length - 1];

                res[0].picture = '/upload/image/' + file_string;
            }

            $('#imgShow').attr("src", res[0].picture);
            $('#imgShow').show();
            return delay(1000);
        } else {
            $('#imgShow').hide();
    
            if(order_id != 1 && order_id == 2){
                responsiveVoice.speak(res[0].read_zh, "Chinese Female");
            }
            
            return delay(2000);
    
        }
    }).then(function(){
        
        // console.log("en title, title2: " + res[0].en_title, res[0].en_title2);

        if(en_title2){
            speak_voice(res[0].read_en, res[0].en_title, res[0].zh_title, res[0].en_title2);    
        } else {
            speak_voice(res[0].read_en, res[0].en_title, res[0].zh_title);    
        }
        

        
        return delay(1000);
    }).then(function(){
        $('#record_voice').show();
        $('#rec_text_01').html(en_title);
        $('#rec_text_02').html(en_title);

        if(en_title2){
            $('#type_word').show();
            $('.type_word').html(en_title2);
            $('.type_word_02').html(en_title_join2);
        } else {
            $('#type_word').show();
            $('.type_word').html(en_title);
            $('.type_word_02').html(en_title_join);
        }
        
    })

}

let delay = function(s){
    return new Promise(function(resolve,reject){
        setTimeout(resolve,s); 
    });
};

function speak2(res){
    responsiveVoice.speak('題組一', "Chinese Female", {onend: speak3(res)})
}

function speak3(res){
    responsiveVoice.speak('使用說明', "Chinese Female", {onend: show_all(res)})
}

function show_all(res){
    if($('#imgShow').attr("src") != ""){
        $('#imgShow').show();
    } else {
        // responsiveVoice.speak('一個(張、隻...等)', 'Chinese Female')
        speak_voice('a', 'a', '一個(張、隻...等)');
        delay_speak(res)
    } 
}

function doesFileExist(urlToFile) {
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlToFile, false);
    xhr.send();
     
    if (xhr.status == "404") {
        return false;
    } else {
        return true;
    }
}

function speak_voice(read_en, en_title, zh_title, en_title2){

    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order_id = new URLSearchParams(window.location.search).get('order');

    file_exists = doesFileExist('/upload/sound_v2/' + topic_no + '_' + order_id + '.m4a');
    
    $('#eng_text_voice').html(en_title);
    en_title = en_title.trim();
    zh_title = zh_title.trim();
    
    en_arr = en_title.split("");
    en_title_join = en_arr.join(' ');
    en_title_str = en_arr.join(" ");

    if(en_title2){
        en_arr2 = en_title2.split("");
        en_title_join2 = en_arr2.join(' ');
        en_title_str2 = en_arr2.join(" ");
    }
    $('#eng_text_2time_voice').html(en_title_join + ' ; ' + en_title + '<br>' + en_title_join + ' ; ' + en_title);
    if(en_title2){
        $('#eng_text_2time_voice').html(en_title2 );
    }
    $('#zh_text_voice').html(zh_title);
    $('#zh_eng_voice').html(en_title + ' ; ' + zh_title);

    if(file_exists == true){
        // $('#media').attr("src", '/upload/sound_v2/' + topic_no + '_' + order_id + '.m4a');
        $('#media_mp3').attr("src", '/upload/sound_v2/' + topic_no + '_' + order_id + '.m4a');
    } else {
        
        console.log('read en: ' +read_en);

        // console.log(topic_no, order_id);
        if(en_title2){
            var data = [
                {'text': en_title, 'lang': "US English Female", 'rate': 0.8, 
                 'timeout': 1.5, 'rec': false, 'rec_show': ''},
                {'text': read_en , 'lang': "US English Female", "rate": 0.8, 
                'timeout': 1.5, 'rec': false, 'rec_show': ''},
                
                {'text': zh_title, 'lang': "Chinese Female", "rate": 1.0, 
                 'timeout':0.1,'rec': false, 'rec_show': ''},
                {'text': en_title, 'lang': 'US English Female', "rate": 0.5 , 
                 'timeout': 0.1,'rec': false, 'rec_show': ''},
                {'text': zh_title, 'lang': 'Chinese Female', "rate": 1.0, 
                 'timeout': 0.1,'rec': false, 'rec_show': ''},
            ];
        } else {
            var data = [
                {'text': en_title, 'lang': "US English Female", 'rate': 0.8, 
                 'timeout': 1.5, 'rec': false, 'rec_show': ''},
                {'text': read_en + ',' + en_title , 'lang': "US English Female", "rate": 0.3, 
                'timeout': 3, 'rec': false, 'rec_show': ''},
                {'text': read_en + ',' + en_title, 'lang': "US English Female", "rate": 0.3, 
                 'timeout': 3, 'rec': false, 'rec_show': ''},
                {'text': zh_title, 'lang': "Chinese Female", "rate": 1.0, 
                 'timeout':0.1,'rec': false, 'rec_show': ''},
                {'text': en_title, 'lang': 'US English Female', "rate": 0.5 , 
                 'timeout': 0.1,'rec': false, 'rec_show': ''},
                {'text': zh_title, 'lang': 'Chinese Female', "rate": 1.0, 
                 'timeout': 0.1,'rec': false, 'rec_show': ''},
            ];
        }
        
        var j = data.length;
    
        function step2(i){
            return new Promise(function(resolve, reject){
                if (typeof data[i] == 'undefined'){
                        reject("out of rang");
                } else {               
                    setTimeout(function(){
                        if(order_id != 1 || order_id != 2){
                            responsiveVoice.speak(data[i].text, data[i].lang, { rate: data[i].rate, onend: resolve });
                        } else {
                            responsiveVoice.speak('', data[i].lang, { rate: data[i].rate, onend: resolve });
                        }
                        
    
                        if(data[i].rec_show != ''){
                            let focus = $('#' + data[i].rec_show);
                            focus.focus();
                        }
    
                        
                    }, data[i].timeout * 1000);      
                    
                }
            })
            
        }
    
        (async () => {
            for (let i = 0; i < j; i++) {
                await step2(i);
            }
        })();
    } 
     
}

function text_display(){
    $('#lblContent').show();
}

function question_prev() {
    if(index == 0) return;
    question_break = true;
    index--;
    // question_start();
}

function question_next() {
    if(index == question.length - 1) return;
    question_break = true;
    index++;
    // question_start();
}

function sleep(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, ms);
    })
}

function stopRecording(button) {

    recognition.stop();

    button.disabled = true;
    button.previousElementSibling.disabled = false;
}


function startRecording(button) {

    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');
    $('#download_1').empty();

    // button.disabled = true;
    // button.nextElementSibling.disabled = false;
    
    // get output div reference
    var output = document.getElementById("download_1");
    // get action element reference
    var action = document.getElementById("action");
    // new speech recognition object
    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
    var recognition = new SpeechRecognition();

    // This runs when the speech recognition service starts
    recognition.onstart = function() {
        button.disabled = true;
        // action.innerHTML = "<small>listening, please speak...</small>";
    };
    
    recognition.onspeechend = function() {
        // action.innerHTML = "<small>stopped listening, hope you are done...</small>";
        button.disabled = false;
        recognition.stop();
    }
    
    // This runs when the speech recognition service returns result
    recognition.onresult = function(event) {

        var transcript = event.results[0][0].transcript;
        var confidence = event.results[0][0].confidence;
        output.innerHTML = "<b>語音轉文字：</b> " + transcript;
        output.classList.remove("hide");

        $.post('/ajax/save_voice_to_text/' + topic_no + '/' + order , 
            {
                'level' : 1,
                'transcript': transcript,
                'number'    : 1
            }, function(){

        })

    };
    
        // start recognition
        recognition.start();
}

function startRecording2(button) {

    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');
    $('#download_2').empty();

    // button.disabled = true;
    // button.nextElementSibling.disabled = false;

    // get output div reference
    var output = document.getElementById("download_2");
    // get action element reference
    var action = document.getElementById("action");
    // new speech recognition object
    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
    var recognition = new SpeechRecognition();

    // This runs when the speech recognition service starts
    recognition.onstart = function() {
        // action.innerHTML = "<small>listening, please speak...</small>";
        button.disabled = true;
    };

    recognition.onspeechend = function() {
        // action.innerHTML = "<small>stopped listening, hope you are done...</small>";
        button.disabled = false;
        recognition.stop();
    }

    // This runs when the speech recognition service returns result
    recognition.onresult = function(event) {
        var transcript = event.results[0][0].transcript;
        var confidence = event.results[0][0].confidence;
        output.innerHTML = "<b>語音轉文字：</b> " + transcript;
        output.classList.remove("hide");

        
        $.post('/ajax/save_voice_to_text/' + topic_no + '/' + order, 
            {
                'level' : 1,
                'transcript': transcript,
                'number'    : 2
            }, function(){

        })
    };

        // start recognition
        recognition.start();
}


// 錄製聲音(暫時用不到)
/*
function __log(e, data) {
    log.innerHTML += "\n" + e + " " + (data || '');
}

var audio_context;
var recorder;

function startUserMedia(stream) {
    var input = audio_context.createMediaStreamSource(stream);
    __log('Media stream created.');

    // Uncomment if you want the audio to feedback directly
    //input.connect(audio_context.destination);
    //__log('Input connected to audio context destination.');
    
    recorder = new Recorder(input);
    __log('Recorder initialised.');
}

function startRecording(button) {
    recorder && recorder.record();
    button.disabled = true;
    button.nextElementSibling.disabled = false;
    __log('Recording...');
}

function stopRecording(button) {
    check_audio();
    recorder && recorder.stop();
    button.disabled = true;
    button.previousElementSibling.disabled = false;
    __log('Stopped recording.');
    
    // create WAV download link using audio data blob
    createDownloadLink();
    
    recorder.clear();

}

function stopRecording2(button) {
    check_audio();
    recorder && recorder.stop();
    button.disabled = true;
    button.previousElementSibling.disabled = false;
    __log('Stopped recording.');
    
    // create WAV download link using audio data blob
    createDownloadLink2();
    
    recorder.clear();
    
}

function createDownloadLink() {
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');

    recorder && recorder.exportWAV(function(blob) {

        data = new FormData();
        data.append('audio', blob);
        data.append('file', 'audio_1')
        $.ajax({
            url: "{{ url('ajax/save_audio') }}/" + topic_no + '/' + order,
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            
        }).done(function (){
            console.log('Success');
        })

    }, "audio/mp3");

    check_audio();
}

function createDownloadLink2() {
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');
    recorder && recorder.exportWAV(function(blob) {

        data = new FormData();
        data.append('audio', blob);
        data.append('file', 'audio_2');
        $.ajax({
            url: "{{ url('ajax/save_audio') }}/" + topic_no + '/' + order,
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            
        }).done(function (){
            console.log('Success');
        })

    }, "audio/mp3");

    check_audio();
}

window.onload = function init() {
    // try {
    // // webkit shim
    // window.AudioContext = window.AudioContext || window.webkitAudioContext;
    // navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
    // window.URL = window.URL || window.webkitURL;
    
    // audio_context = new AudioContext;
    // __log('Audio context set up.');
    // __log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
    // } catch (e) {
    // alert('No web audio support in this browser!');
    // }
    
    // navigator.getUserMedia({audio: true}, startUserMedia, function(e) {
    // __log('No live audio input: ' + e);
    // });
};
*/

let urlParams = new URLSearchParams(window.location.search);
// console.log(urlParams.get('time')); 

// 計時器功能
// let start = new Date;
// setInterval(function() {
//     let miliseconds = new Date - start;
//     let totalSeconds = Math.floor(miliseconds/1000); 
//     let hours = Math.floor(totalSeconds/60/60);
//     let minutes = Math.floor(totalSeconds/60); 
//     let seconds = totalSeconds - minutes * 60; 
    
//     $('#lblTime').html(hours + ":" + minutes + ":" + seconds);
    
//     if(urlParams.has('time') == '' || urlParams.get('time') == ''){
//         time = 0;       
//     } else {
//         time = urlParams.get('time')
//     }

//     let totalSeconds2 = parseInt(totalSeconds) + parseInt(time);
//     let hours2 = Math.floor(totalSeconds2/60/60);
//     let minutes2 = Math.floor(totalSeconds2/60); 
//     let seconds2 = totalSeconds2 - minutes2 * 60; 
    
//     $('#lblTimeAll').html(hours2 + ":" + minutes2 + ':' + seconds2);
//     $('#lblTimeAll_sec').val(totalSeconds2);
// }, 1000)


$(document).keydown(function(event){
    
});

timeout();

function timeout() {
    start_time = new Date;
    setInterval(function(){
            let miliseconds = new Date - start_time;
            let seconds =  miliseconds / 1000; 

            if(seconds == (60 * 3)){
                console.log('first 3 minutes');
            }
            if(seconds == (60 * 6)){
                console.log('first 3 minutes');
            }
    }, 1000);
}


  


