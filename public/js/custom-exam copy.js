let topic_no;
let question;
let question_index;
let question_break;

$(function() {

    // clearTimeout();
    
    // if(!user_valid()) return;
   
    // mode_enter();

    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');
    $('#lblTopic').text('題組 - '+ topic_no);
    topic_get(topic_no, order);

    // check_audio();
    
});



$(document).on('keypress', function(e) {
    console.log(e);

    // 回選擇頁面
    if(e.code === 'KeyQ' && e.ctrlKey) { 
        // console.log('page_back');
        window.location.href = '/topic';
    }

    // Shift key + c.code Degit1
    if(e.shiftKey  && e.code == 'KeyS'){
        console.log(e.code);
        $('#rec_button_start_1').trigger('click');
    }

    if(e.shiftKey  && e.code == 'KeyD'){
        console.log(e.code);
        $('#rec_button_start_2').trigger('click');
    }

    if(e.shiftKey  && e.code == 'KeyH'){
        console.log(e.code);
        responsiveVoice.pause();
    }
    
    // if(e.shiftKey  && e.code == 'KeyF'){
    //     console.log(e.code);
    //     responsiveVoice.resume();
    // }
});

responsiveVoice.clickEvent();
responsiveVoice.setDefaultVoice("Chinese Female");

$('.button_click').on('focus', function(){
    responsiveVoice.speak($(this).text(), "Chinese Female");
})

$('.button_rec').on('focus', function(){
    responsiveVoice.speak('請說下列單字的英文', "Chinese Female");
})

$('.button_rec2').on('focus', function(){
    responsiveVoice.speak('請說', "Chinese Female");
})

$('#type_01').on('focus', function(){
    responsiveVoice.speak('請輸入', "Chinese Female");
    responsiveVoice.speak($('.type_word').html(), "US English Female");
});



// 檢查 audio (可能用不到)


function user_signOut() {
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order_id = new URLSearchParams(window.location.search).get('order');
    let type_01 = $('#type_01').val();
    let type_02 = '';
    let type_03 = '';
    let order_time = $('#lblTime').html();
    let ttl_time= $('#lblTimeAll').html();
    let level = 3;
    save_to_server(topic_no, order_id, type_01, type_02, type_03, order_time, ttl_time, level);
    window.location.href = '/logout';
}

function user_help() {
    window.location.href = '/help';
}

$(document).on('keypress', function(e){

})

let delay_prev = function(s) {
    return new Promise(function(resolve, reject){
        setTimeout(resolve, s)
    })
}

let delay_next = function(s) {
    return new Promise(function(resolve, reject){
        setTimeout(resolve, s)
    })
}

function topic_get(topic_no, order_id) {

    $.ajax({
        url: '/topic/request',
        type: 'GET',
        data: {
            'tid': topic_no,
            'order': order_id
        },
        // data: JSON.stringify(reqData),
        beforeSend: function() {
            
        },
        error: function(err) {
            console.log(err)
            // console.log('Ajax Request Error');
        },
        success: function(res) {
            console.log(res);

            let en_title = res[0].en_title;
            let zh_title = res[0].zh_title;
            let read_en = res[0].read_en;
            let level = 3;

            en_title = en_title.trim();
            en_arr = en_title.split("");
            en_title_join = en_arr.join(',');
            en_title_str = en_arr.join(" ");
            read_en = read_en;

            $('#lblTopicNum').html(res[0].order + '/' + res.count);

            $('#lblContent').hide();

            $('#num').html(res[0].order);

            if(res[0].order == 1){
                $.post('/ajax/delete_server', 
                {
                    'level': level
                }, function(e){

                })
            }

            if(res.prev_page != ''){
                $('#prev_page').on('click', function() {
                    // $(this).prop('data', 'learn?' + res.prev_page + '&time=' + $('#lblTimeAll').html());
                    
                    let type_01 = $('#type_01').val();
                    let type_02 = $('#type_02').val();
                    let type_03 = $('#type_03').val();
                    let order_time = $('#lblTime').html();
                    let ttl_time = $('#lblTimeAll').html();

                    delay_prev().then(function() {
                        save_to_server(topic_no, order_id, type_01, type_02, type_03, order_time, ttl_time, level);
                        
                        return delay_prev(1500);
                    }).then(function(){
                        window.location = 'exam?' + res.prev_page + '&time=' + $('#lblTimeAll_sec').val();
                    })

                })
            }
            
            if(res.next_page != ''){
                $('#next_page').on('click', function() {
                    // $(this).prop('data', 'learn?' + res.prev_page + '&time=' + $('#lblTimeAll').html());
                    let type_01 = $('#type_01').val();
                    let type_02 = $('#type_02').val();
                    let type_03 = $('#type_03').val();
                    let order_time = $('#lblTime').html();
                    let ttl_time = $('#lblTimeAll').html();
                    
                    delay_next().then(function() {
                        save_to_server(topic_no, order_id, type_01, type_02, type_03, order_time, ttl_time, level);
                        
                        return delay_next(1500);
                    }).then(function(){
                        window.location = 'exam?' + res.next_page + '&time=' + $('#lblTimeAll_sec').val();
                    })
                })
            }

            if(res[0].en_title != null){
                $('#lblContent').html(res[0].en_title)
            }

            if(res[0].order == 1){
                responsiveVoice.speak('學習模式', "Chinese Female", {onend: speak2(res)})   
                
            } else {
                
                delay_speak(res);

            }                

        }
    })
}

function save_to_server(){
    let args = Array.from(arguments);

    console.log(args[0]);

    $.post('/ajax/save_server', {
        'topic': args[0],
        'order': args[1],
        'type_01': args[2],
        'type_02': args[3],
        'type_03': args[4],
        'order_time': args[5],
        'ttl_time': args[6],
        'level': args[7]
    }, function(e){
        console.log(e);
        
    });
}

function delay_speak(res){
    console.log(res);

    let en_title = res[0].en_title;
    let zh_title = res[0].zh_title;
    let read_en = res[0].read_en;

    en_title = en_title.trim();
    en_arr = en_title.split("");
    en_title_join = en_arr.join(',');
    en_title_str = en_arr.join(" ");
    read_en = read_en;
    delay().then(function(){
        if(res[0].sound != null){
            $('#media').attr("src", '/sounds/' +res[0].sound);
            $('#media_mp3').attr("src", '/sounds/' + res[0].sound);
        }     
        return delay(1000);
    }).then(function(){
        if(res[0].picture != null){
            $('#imgShow').attr("src", res[0].picture);
            $('#imgShow').show();
            return delay(1000);
        } else {
            $('#imgShow').hide();
    
            responsiveVoice.speak(res[0].read_zh, "Chinese Female");
            return delay(2000);
    
        }
    }).then(function(){
        speak_voice(res[0].read_en, res[0].en_title, res[0].zh_title);                    
        return delay(1000);
    }).then(function(){
        $('#record_voice').show();
        $('#rec_text_01').html(zh_title);
        // $('#rec_text_02').html(en_title);

        $('#type_word').show();
        $('.type_word').html(zh_title);
        $('.type_word_02').html(en_title_join);
    })

}

let delay = function(s){
    return new Promise(function(resolve,reject){
        setTimeout(resolve,s); 
    });
};

function speak2(res){
    responsiveVoice.speak('題組一', "Chinese Female", {onend: speak3(res)})
}

function speak3(res){
    responsiveVoice.speak('使用說明', "Chinese Female", {onend: show_all(res)})
}

function show_all(res){
    if($('#imgShow').attr("src") != ""){
        $('#imgShow').show();
    } else {
        // responsiveVoice.speak('一個(張、隻...等)', 'Chinese Female')
        speak_voice('a', 'a', '一個(張、隻...等)，子音之前');
        delay_speak(res)
    } 
}



function speak_voice(read_en, en_title, zh_title){
    en_title = en_title.trim();
    zh_title = zh_title.trim();
    $('#eng_text_voice').html(en_title);
    en_arr = en_title.split("");
    en_title_join = en_arr.join(' ');
    en_title_str = en_arr.join(" ");
    $('#eng_text_2time_voice').html(en_title_join + ' ; ' + en_title + '<br>' + en_title_join + ' ; ' + en_title);
    $('#zh_text_voice').html(zh_title);
    $('#zh_eng_voice').html(en_title + ' ; ' + zh_title);

    var data = [
        {'text': en_title, 'lang': "US English Female", 'rate': 0.8, 'timeout': 1.5, 'rec': false, 'rec_show': ''},
        // {'text': read_en + ";" + en_title, 'lang': "US English Female", "rate": 0.6, 'timeout': 1.5, 'rec': false, 'rec_show': ''},
        // {'text': read_en + ";" + en_title, 'lang': "US English Female", "rate": 0.6, 'timeout': 1.5, 'rec': false, 'rec_show': ''},
        {'text': zh_title, 'lang': "Chinese Female", "rate": 1.0, 'timeout': 1.5,'rec': false, 'rec_show': ''},
        // {'text': en_title, 'lang': 'US English Female', "rate": 0.6 , 'timeout': 0.1,'rec': false, 'rec_show': ''},
        // {'text': zh_title, 'lang': 'Chinese Female', "rate": 1.0, 'timeout': 0.1,'rec': false, 'rec_show': ''},
        // {'text': '請跟我說', 'lang': 'Chinese Female', 'rate': 1.0, 'timeout': 1,'rec': false, 'rec_show': ''},
        // {'text': en_title, 'lang': 'US English Female', 'rate': 1.0, 'timeout': 2,'rec': true, 'rec_show': 'rec_01'},
        // {'text': en_title, 'lang': 'US English Female', 'rate': 1.0, 'timeout': 2,'rec': true, 'rec_show': 'rec_02'},
        // {'text': '請輸入', 'lang': 'Chinese Female', 'rate': 1.0, 'timeout': 0.1,'rec': false, 'rec_show': ''},
        // {'text': en_title, 'lang': 'US English Female', 'rate': 1.0, 'timeout': 4,'rec': false, 'rec_show': 'type_01'},
        // {'text': '請輸入', 'lang': 'Chinese Female', 'rate': 1.0, 'timeout': 0.1,'rec': false, 'rec_show': ''},
        // {'text': en_title_str, 'lang': 'US English Female', 'rate': 0.1, 'timeout': 4,'rec': false, 'rec_show': 'type_02'},
        // {'text': '請輸入', 'lang': 'Chinese Female', 'rate': 1.0, 'timeout': 0.1,'rec': false, 'rec_show': ''},
        // {'text': en_title, 'lang': 'US English Female', 'rate': 1.0, 'timeout': 4,'rec': false, 'rec_show': 'type_03'},
    ];

    var j = data.length;

    function step2(i){
        return new Promise(function(resolve, reject){
            if (typeof data[i] == 'undefined'){
                    reject("out of rang");
            } else {               
                setTimeout(function(){
                    responsiveVoice.speak(data[i].text, data[i].lang, { rate: data[i].rate, onend: resolve });

                    if(data[i].rec_show != ''){
                        let focus = $('#' + data[i].rec_show);
                        focus.focus();
                    }

                }, data[i].timeout * 1500);      
                
            }
        })
        
    }

    (async () => {
        for (let i = 0; i < j; i++) {
            await step2(i);
        }
    })();
     
}

function text_display(){
    $('#lblContent').show();
}

function question_prev() {
    if(index == 0) return;
    question_break = true;
    index--;
    // question_start();
}

function question_next() {
    if(index == question.length - 1) return;
    question_break = true;
    index++;
    // question_start();
}

function sleep(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, ms);
    })
}

function stopRecording(button) {

    recognition.stop();

    button.disabled = true;
    button.previousElementSibling.disabled = false;
}


function startRecording(button) {

    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');
    $('#download_1').empty();

    // button.disabled = true;
    // button.nextElementSibling.disabled = false;
    
    // get output div reference
    var output = document.getElementById("download_1");
    // get action element reference
    var action = document.getElementById("action");
    // new speech recognition object
    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
    var recognition = new SpeechRecognition();

    // This runs when the speech recognition service starts
    recognition.onstart = function() {
        button.disabled = true;
        // action.innerHTML = "<small>listening, please speak...</small>";
    };
    
    recognition.onspeechend = function() {
        // action.innerHTML = "<small>stopped listening, hope you are done...</small>";
        button.disabled = false;
        recognition.stop();
    }
    
    // This runs when the speech recognition service returns result
    recognition.onresult = function(event) {

        var transcript = event.results[0][0].transcript;
        var confidence = event.results[0][0].confidence;
        output.innerHTML = "<b>語音轉文字：</b> " + transcript;
        output.classList.remove("hide");

        $.post('/ajax/save_voice_to_text/' + topic_no + '/' + order , 
            {
                'level' : 3,
                'transcript': transcript,
                'number'    : 1
            }, function(){

        })

    };
    
        // start recognition
        recognition.start();
}

function startRecording2(button) {

    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');
    $('#download_2').empty();

    // button.disabled = true;
    // button.nextElementSibling.disabled = false;

    // get output div reference
    var output = document.getElementById("download_2");
    // get action element reference
    var action = document.getElementById("action");
    // new speech recognition object
    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
    var recognition = new SpeechRecognition();

    // This runs when the speech recognition service starts
    recognition.onstart = function() {
        // action.innerHTML = "<small>listening, please speak...</small>";
        button.disabled = true;
    };

    recognition.onspeechend = function() {
        // action.innerHTML = "<small>stopped listening, hope you are done...</small>";
        button.disabled = false;
        recognition.stop();
    }

    // This runs when the speech recognition service returns result
    recognition.onresult = function(event) {
        var transcript = event.results[0][0].transcript;
        var confidence = event.results[0][0].confidence;
        output.innerHTML = "<b>語音轉文字：</b> " + transcript;
        output.classList.remove("hide");

        
        $.post('/ajax/save_voice_to_text/' + topic_no + '/' + order, 
            {
                'level' : 3,
                'transcript': transcript,
                'number'    : 2
            }, function(){

        })
    };

        // start recognition
        recognition.start();
}


// 錄製聲音(暫時用不到)
/*
function __log(e, data) {
    log.innerHTML += "\n" + e + " " + (data || '');
}

var audio_context;
var recorder;

function startUserMedia(stream) {
    var input = audio_context.createMediaStreamSource(stream);
    __log('Media stream created.');

    // Uncomment if you want the audio to feedback directly
    //input.connect(audio_context.destination);
    //__log('Input connected to audio context destination.');
    
    recorder = new Recorder(input);
    __log('Recorder initialised.');
}

function startRecording(button) {
    recorder && recorder.record();
    button.disabled = true;
    button.nextElementSibling.disabled = false;
    __log('Recording...');
}

function stopRecording(button) {
    check_audio();
    recorder && recorder.stop();
    button.disabled = true;
    button.previousElementSibling.disabled = false;
    __log('Stopped recording.');
    
    // create WAV download link using audio data blob
    createDownloadLink();
    
    recorder.clear();

}

function stopRecording2(button) {
    check_audio();
    recorder && recorder.stop();
    button.disabled = true;
    button.previousElementSibling.disabled = false;
    __log('Stopped recording.');
    
    // create WAV download link using audio data blob
    createDownloadLink2();
    
    recorder.clear();
    
}

function createDownloadLink() {
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');

    recorder && recorder.exportWAV(function(blob) {

        data = new FormData();
        data.append('audio', blob);
        data.append('file', 'audio_1')
        $.ajax({
            url: "{{ url('ajax/save_audio') }}/" + topic_no + '/' + order,
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            
        }).done(function (){
            console.log('Success');
        })

    }, "audio/mp3");

    check_audio();
}

function createDownloadLink2() {
    let topic_no = new URLSearchParams(window.location.search).get('tid');
    let order = new URLSearchParams(window.location.search).get('order');
    recorder && recorder.exportWAV(function(blob) {

        data = new FormData();
        data.append('audio', blob);
        data.append('file', 'audio_2');
        $.ajax({
            url: "{{ url('ajax/save_audio') }}/" + topic_no + '/' + order,
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            
        }).done(function (){
            console.log('Success');
        })

    }, "audio/mp3");

    check_audio();
}

window.onload = function init() {
    // try {
    // // webkit shim
    // window.AudioContext = window.AudioContext || window.webkitAudioContext;
    // navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
    // window.URL = window.URL || window.webkitURL;
    
    // audio_context = new AudioContext;
    // __log('Audio context set up.');
    // __log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
    // } catch (e) {
    // alert('No web audio support in this browser!');
    // }
    
    // navigator.getUserMedia({audio: true}, startUserMedia, function(e) {
    // __log('No live audio input: ' + e);
    // });
};
*/

let urlParams = new URLSearchParams(window.location.search);
// console.log(urlParams.get('time')); 

// 計時器功能
let start = new Date;
setInterval(function() {
    let miliseconds = new Date - start;
    let totalSeconds = Math.floor(miliseconds/1000); 
    let hours = Math.floor(totalSeconds/60/60);
    let minutes = Math.floor(totalSeconds/60); 
    let seconds = totalSeconds - minutes * 60; 
    
    $('#lblTime').html(hours + ":" + minutes + ":" + seconds);
    
    if(urlParams.has('time') == '' || urlParams.get('time') == ''){
        time = 0;       
    } else {
        time = urlParams.get('time')
    }

    let totalSeconds2 = parseInt(totalSeconds) + parseInt(time);
    let hours2 = Math.floor(totalSeconds2/60/60);
    let minutes2 = Math.floor(totalSeconds2/60); 
    let seconds2 = totalSeconds2 - minutes2 * 60; 
    
    $('#lblTimeAll').html(hours2 + ":" + minutes2 + ':' + seconds2);
    $('#lblTimeAll_sec').val(totalSeconds2);
}, 1000)


$(document).keydown(function(event){
    
});

timeout();

function timeout() {
    start_time = new Date;
    setInterval(function(){
            let miliseconds = new Date - start_time;
            let seconds =  miliseconds / 1000; 

            if(seconds == (60 * 3)){
                console.log('first 3 minutes');
            }
            if(seconds == (60 * 6)){
                console.log('first 3 minutes');
            }
    }, 1000);
}

// // 閒置的時間
let oTimerId;
function Timeout(){
    $('#media').attr('src', '/sound/notify2.wav');
    $('#media_mp3').attr("src", '/sounds/notify2.wav');
    alert("\n您好\n您可能已離開!\n很抱歉!!請重新登入系統\n謝謝!!");
    location.href= ('/logout');
}
function ReCalculate(){
    clearTimeout(oTimerId);
    oTimerId = setTimeout('Timeout()', 360 * 1000); //js 是用毫秒計算
}


document.onkeydown = ReCalculate;
// // document.onmousemove = ReCalculate;
ReCalculate();    
